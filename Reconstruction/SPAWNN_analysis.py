#!/usr/bin/python3
  
####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################

#### Python imports
import sys, os
import argparse
import csv
import glob
import h5py
import json
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
from pathlib import Path
import seaborn as sns
import time
from tqdm import tqdm

#### Self functions imports
sys.path.append('../Tools')
import tools

#### Keras imports
import keras
import keras.backend as K
from keras.models import load_model
from keras.models import model_from_json


def main():

#### Retrieving the arguments
    parser = argparse.ArgumentParser(description='Generate dataset.')
    parser.add_argument('-i', '--intput', dest='in_file',required=True)		# Invivo file.h5 to analyse
    parser.add_argument('-o', '--output', dest='out_file',required=True)	# Output file.h5 path
    parser.add_argument('-mp', '--ModelParameters', dest='model_pm', required=True)		# Model parameters folder path
    parser.add_argument('-mb', '--ModelMetabolites', dest='model_mb', required=True)		# Model parameters folder path
    parser.add_argument('-bl', '--ModelBaseline', dest='model_bl', required=True)		# Model baseline folder path
    parser.add_argument('-v', dest='verbose', action='store_true', help='Verbose output') # Use if you want print output

    args = parser.parse_args()
    in_file  = args.in_file 
    out_file = args.out_file
    model_pm = args.model_pm
    model_mb = args.model_mb
    model_bl = args.model_bl
    verbose  = args.verbose

#### Load the model parameters
    model_parameters = tools.load_h5_file("%s/params.h5" %(model_pm),
                                         load=('Model/y_span',
                                               'Model/y_min',
                                               'Model/Fs',
                                               'Model/Npt',
                                               'Model/NMRFreq',
                                               'Model/WINDOW_START', 
                                               'Model/WINDOW_END',
                                               'Model/N1',
                                               'Model/N2',
                                               'Model/names'),
                                         verbose = verbose)


    y_span_pm 	      = model_parameters['Model/y_span']
    y_min_pm	      = model_parameters['Model/y_min']
    Fs		      = model_parameters['Model/Fs']
    Npt		      = model_parameters['Model/Npt']
    NMRFreq  	      = model_parameters['Model/NMRFreq']
    WINDOW_START      = model_parameters['Model/WINDOW_START']
    WINDOW_END 	      = model_parameters['Model/WINDOW_END']
    N1		      = model_parameters['Model/N1']
    N2		      = model_parameters['Model/N2']
    names_ascii_pm    = model_parameters['Model/names']
    names_utf8_pm     = tools.decode_array_ascii(model_parameters['Model/names'])


    model_metabolites = tools.load_h5_file("%s/params.h5" %(model_mb),
                                         load=('Model/y_span',
                                               'Model/y_min',
                                               'Model/Fs',
                                               'Model/Npt',
                                               'Model/NMRFreq',
                                               'Model/WINDOW_START', 
                                               'Model/WINDOW_END',
                                               'Model/N1',
                                               'Model/N2',
                                               'Model/names'),
                                         verbose = verbose)


    y_span_mb 	  = model_metabolites['Model/y_span']
    y_min_mb	  = model_metabolites['Model/y_min']
    names_ascii   = model_metabolites['Model/names']
    names_utf8_mb = tools.decode_array_ascii(model_metabolites['Model/names'])


    model_baseline = tools.load_h5_file("%s/params.h5" %(model_bl),
                                        load=('Model/pad_beg',
                                        'Model/pad_end'),
                                        verbose = verbose)

    pad_beg = model_baseline['Model/pad_beg']
    pad_end = model_baseline['Model/pad_end']


    ppm = np.linspace(WINDOW_END, WINDOW_START, Npt)[-N2:-N1]
    print('Windowing between {}ppm and {}ppm from total length {}pts.'.format(np.round(ppm[0],0) ,np.round(ppm[-1],0),N2-N1))


#### Load the datas for parameters and concentration prediction
    '''
    This part is for loading my data, you can addapt this part with your data    This part is for loading my data, you can addapt this part with your data.
    '''
    t = time.time()
    temp_datas = {} 
    f = h5py.File(in_file, 'r') 
    for k, v in f.items(): 
        if k!="kdata": continue 
        temp_datas[k] = np.transpose(np.array(v)) 
    data_array = np.zeros(temp_datas['kdata'].shape, dtype=np.complex128)    
    data_array[:] = temp_datas['kdata'][:]['real'] + 1j*temp_datas['kdata'][:]['imag']
    '''
    Data is is assumed to be either FID: (time data, averages) or MRSI: (time data, kx, ky, averages, kz)
    '''



    if len(data_array.shape)==5: 
        Data_t = np.mean(data_array, axis=3)
        for ii in [1,2,3]:
            Data_t = np.fft.fftshift(np.fft.fft(np.fft.fftshift(Data_t, axes=ii),axis=ii),axes=ii)
        Data_t = np.transpose(Data_t, (1,2,3,0)) # (x, y, z, time data)
    elif len(data_array.shape)==2:
        Data_t = np.mean(data_array, axis=1)
    else:
        raise TypeError("Loading data intended for 1D and 3D data. Please, adapt script for your specific data.")


    DataDim = np.ndim(Data_t)-1
    Data_f_full = np.fft.fftshift(np.fft.fft(Data_t[..., :Npt], axis=DataDim), axes=DataDim)
    Data_f = Data_f_full[..., N1:N2]

    spectra_data = np.stack((np.real(Data_f), np.imag(Data_f)), axis=-1)
    spectra_data_energy = np.sum(np.abs(Data_f) ** 2, axis=DataDim).mean()
    spectra_data *= np.sqrt(1.0/ spectra_data_energy)

    # Saving the shape of the original datas before stacking
    image_shape = Data_f.shape
    spectra_data = spectra_data.reshape((-1, N2-N1, 2)) #stack the x_y_z dim here
    spectra_data = np.pad(spectra_data, ((0,0),(5, 5),(1,0)), 'wrap')
    spectra_data = spectra_data[..., None]
    print('Time for loading data = '+str(np.round(time.time() - t, decimals=2))+'s.\n\n')

    #Predict parameters
    t = time.time()
    json_file_pm = open("%s/model.json" %(model_pm), 'r')
    load_model_pm_json = json_file_pm.read()
    json_file_pm.close()
    
    load_model_pm = model_from_json(load_model_pm_json)
    load_model_pm.load_weights("%s/weights.h5" %(model_pm))
    load_model_pm.compile(loss='mse', optimizer='adam', metrics=[tools.R2])
    
    predictions_pm = load_model_pm.predict(spectra_data)
    predictions_pm = predictions_pm * y_span_pm + y_min_pm
    pred_param     = np.reshape(predictions_pm,image_shape[:-1] + (-1,))
    print('\n\nPredictions for parameters  have been made.')
    print('Evaluation time = '+str(np.round(time.time() - t, decimals=2))+'s.\n\n')

    #Predict metabolites
    t = time.time()
    json_file_mb = open("%s/model.json" %(model_mb), 'r')
    load_model_mb_json = json_file_mb.read()
    json_file_mb.close()
    
    load_model_mb = model_from_json(load_model_mb_json)
    load_model_mb.load_weights("%s/weights.h5" %(model_mb))
    load_model_mb.compile(loss='mse', optimizer='adam', metrics=[tools.R2])
    
    predictions_mb = load_model_mb.predict(spectra_data)
    predictions_mb = predictions_mb * y_span_mb + y_min_mb
    pred_metab     = np.reshape(predictions_mb,image_shape[:-1] + (-1,))
    print('\n\nPredictions for metabolites concentrations have been made.')
    print('Evaluation time = '+str(np.round(time.time() - t, decimals=2))+'s.\n\n')
    pred_metab[pred_metab<0] = 0.



#### Load the datas for baseline prediction
    spectra_baseline = np.stack((np.real(Data_f), np.imag(Data_f)), axis=-1)
    spectra_baseline *= np.sqrt(1.0/ spectra_data_energy)
    spectra_baseline = spectra_baseline.reshape((-1, N2-N1, 2)) 

    spectra_baseline = np.pad(spectra_baseline, ((0,0),(pad_beg, pad_end),(0,0)), 'edge')    
    spectra_baseline = np.pad(spectra_baseline, ((0,0),(0, 0),(1,0)), 'wrap')
    spectra_baseline = np.expand_dims(spectra_baseline, axis = -1)

    #Predict model for baseline
    t = time.time()
    json_file_bl = open("%s/model.json" %(model_bl), 'r')
    load_model_bl_json = json_file_bl.read()
    json_file_bl.close()

    load_model_bl = model_from_json(load_model_bl_json)
    load_model_bl.load_weights("%s/weights.h5" %(model_bl))
    load_model_bl.compile(loss='mse', optimizer='adam', metrics=['mse'])

    predictions_bl = load_model_bl.predict(spectra_baseline) 
    pred_bl        = np.squeeze(predictions_bl[:,pad_beg:(np.shape(predictions_bl)[1]-pad_end),0]+ 1j*predictions_bl[:,pad_beg:(np.shape(predictions_bl)[1]-pad_end),1])
    pred_bl        = np.reshape(pred_bl,image_shape[:-1] + (-1,))
    pred_bl *= np.sqrt(spectra_data_energy)
    print('\n\nPredictions for baseline have been made.')
    print('Evaluation time = '+str(np.round(time.time() - t, decimals=2))+'s.\n\n')
    

#### Writing the output datas
    with h5py.File(out_file, 'w', libver='earliest') as f:
        ResCNN = f.create_group('CNN_Results')
        ResCNN.create_dataset('Conc', data=pred_metab)
        ResCNN.create_dataset('Parameters', data=pred_param)
        ResCNN.create_dataset('ParamNames', data=names_ascii_pm)
        ResCNN.create_dataset('MetabNames', data=names_ascii)
        ResCNN.create_dataset('Fs', data=Fs)
        ResCNN.create_dataset('Npt', data=Npt)
        ResCNN.create_dataset('Baseline', data=pred_bl)
        ResCNN.create_dataset('NMRFreq', data=NMRFreq)
        ResCNN.create_dataset('WINDOW_START', data=WINDOW_START)
        ResCNN.create_dataset('WINDOW_END', data=WINDOW_END)
        ResCNN.create_dataset('N1', data=N1)
        ResCNN.create_dataset('N2', data=N2) 
        ResCNN.create_dataset('Data_f', data=Data_f) 
        ResCNN.create_dataset('Data_f_full', data=Data_f_full) 
        ResCNN.create_dataset('Energy', data=spectra_data_energy) 
    print('')
    print('Prediction has been saved at location '+out_file)

    tools.appreciation()




if __name__ == '__main__':
    main()
