#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


#### Python imports
import sys, os
import argparse
import csv
import h5py
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
from pathlib import Path
import pickle
import seaborn as sns
from tqdm import tqdm

#### Self functions imports
sys.path.append('../Tools')
import tools

#### Keras imports
import keras
import keras.backend as K
from keras.models import load_model
from keras.models import model_from_json

def main():

#### Retrieving the arguments
    parser = argparse.ArgumentParser(description='Read CNN prediction.')
    parser.add_argument('-i', '--intput', dest='in_file',required=True)         # Invivo file.h5 to analyse
    parser.add_argument('-o', '--output', dest='out_folder',required=True)         # Output folder path
    parser.add_argument('-v', dest='verbose', action='store_true', help='Verbose output') # Use if you want print output

    args = parser.parse_args()
    in_file  = args.in_file
    out_folder = args.out_folder
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    verbose  = args.verbose


#### Loading input file
    input_param = tools.load_h5_file(in_file,
                                     load=('CNN_Results/Baseline',
                                           'CNN_Results/Conc',
                                           'CNN_Results/Fs',
                                           'CNN_Results/MetabNames',
                                           'CNN_Results/ParamNames',
                                           'CNN_Results/Npt',
                                           'CNN_Results/Parameters',
                                           'CNN_Results/NMRFreq',
                                           'CNN_Results/WINDOW_START',
                                           'CNN_Results/WINDOW_END',
                                           'CNN_Results/N1',
                                           'CNN_Results/N2',
                                           'CNN_Results/Data_f',
                                           'CNN_Results/Data_f_full',
                                           'CNN_Results/Energy'),
                                     verbose=verbose)

    # Constantes
    Fs            = input_param['CNN_Results/Fs']
    Npt           = input_param['CNN_Results/Npt']
    NMRFreq       = input_param['CNN_Results/NMRFreq']
    WINDOW_START  = input_param['CNN_Results/WINDOW_START']
    WINDOW_END    = input_param['CNN_Results/WINDOW_END']
    N1            = input_param['CNN_Results/N1']
    N2            = input_param['CNN_Results/N2']
    Energy        = input_param['CNN_Results/Energy']

    # Arrays
    Parameters     = input_param['CNN_Results/Parameters'][:]
    Baseline       = input_param['CNN_Results/Baseline'][:]
    Concentration  = input_param['CNN_Results/Conc'][:]
    MetabNames     = input_param['CNN_Results/MetabNames'][:]
    ParamNames     = input_param['CNN_Results/ParamNames'][:]
    Data_f      = input_param['CNN_Results/Data_f'][:]
    Data_f_full = input_param['CNN_Results/Data_f_full'][:]

    ppm = np.linspace(WINDOW_START, WINDOW_END, Npt)[N1:N2] 

#### Getting the normalized concentration value and names
    (x_len, y_len, z_len, nb_met) = Concentration.shape 

    metab_name_str = tools.decode_array_ascii(MetabNames)
    param_name_str = tools.decode_array_ascii(ParamNames)


#### Plotting 3D heatmap
    '''
    The following plot shows an example of heatmap using the mean concentration of ATP in coronal orientation.
    '''
    pdf_path = out_folder+'heatmap.pdf'
    pdf = PdfPages(pdf_path)

    atp_idx = [ii for ii, ss in enumerate(metab_name_str) if 'ATP' in ss] 

    atp_mean = (Concentration[:,:,:,atp_idx[0]]+Concentration[:,:,:,atp_idx[1]]+Concentration[:,:,:,atp_idx[2]])/3
    vmin = np.min(atp_mean)
    vmax = np.max(atp_mean)
    print('')
    with tqdm(total=y_len, desc='Mapping CSI in y-axis') as pbar:
        for kk in range(y_len):
            current_map = atp_mean[:,kk,:]
            fig, ax = plt.subplots() 
            fig.set_size_inches(20, 20)
            plt.title('x-z plan, y='+str(kk), fontsize=20)
            im = ax.imshow(current_map,vmin=vmin, vmax=vmax) 
            ax.set_ylim(len(current_map)-0.5, -0.5)
            cbar = ax.figure.colorbar(im, ax=ax)
            cbar.ax.set_ylabel('Metabolite concentration', rotation=-90, va="bottom", fontsize=20) 
            for i in range(x_len):  
                for j in range(z_len):
                    current_aATP = np.round(Concentration[i,kk,j,atp_idx[0]],3)
                    current_bATP = np.round(Concentration[i,kk,j,atp_idx[1]],3)
                    current_gATP = np.round(Concentration[i,kk,j,atp_idx[2]],3)
                    text = ax.text(i, j,"aATP "+str(current_aATP)+'a.u.\nbATP '+str(current_bATP)+'a.u.\ngATP '+str(current_gATP)+'a.u.',ha="center", va="center", color="w", fontsize=8)
            ax.set_ylabel('x axis', fontsize=20)
            ax.set_xlabel('z axis', fontsize=20)
            ax.invert_yaxis()
            fig.tight_layout()
            pdf.savefig(fig)
            plt.close()
            pbar.update(1)
    pdf.close()

#### Plotting Spectrum
    '''
    The following shows an example of how to reconstruct a spectrum with the evaluated parameters and how to plot on top of the in-vivo spectrum.
    '''

    #geting index of highest atp voxel
    atp_max_idx = np.unravel_index(np.argmax(atp_mean),atp_mean.shape) 
    df =  pd.Series(Parameters[atp_max_idx],index=param_name_str) 

    Spectra, S_meta = tools.creating_spec_from_param(Concentration[atp_max_idx],df,Baseline[atp_max_idx],metab_name_str,Npt,N1,N2,Fs,NMRFreq)

    Data_reph = tools.rephase_spec(Data_f[atp_max_idx],df,Baseline[atp_max_idx],Npt,N1,N2,Fs,True)
    Spec = tools.rephase_spec(Spectra,df,Baseline[atp_max_idx],Npt,N1,N2,Fs,False)

    Spec_meta = np.zeros((S_meta.shape), dtype=np.complex64)
    for ii in range(S_meta.shape[1]):
        Spec_meta[:,ii] = tools.rephase_spec(S_meta[:,ii],df,Baseline[atp_max_idx],Npt,N1,N2,Fs,False)



    tools.plot_raw(Data_f[atp_max_idx],Spectra,Baseline[atp_max_idx],ppm,out_folder)

    tools.plot_short(Data_reph,Spec,ppm,out_folder)

    tools.plot_long(Data_reph,Spec,Spec_meta,metab_name_str,ppm,out_folder)

    tools.appreciation()

if __name__ == '__main__':
    main()

