#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


#### Python imports
import sys, os
import argparse
import csv
import h5py
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
from pathlib import Path
import pickle
import seaborn as sns
from tqdm import tqdm

#### Self functions imports
sys.path.append('../Tools')
import tools

#### Keras imports
import keras
import keras.backend as K
from keras.models import load_model
from keras.models import model_from_json

def main():

#### Retrieving the arguments
    parser = argparse.ArgumentParser(description='Read CNN prediction.')
    parser.add_argument('-i', '--intput', dest='in_file',required=True)         # Invivo file.h5 to analyse
    parser.add_argument('-o', '--output', dest='out_folder',required=True)         # Output folder path
    parser.add_argument('-v', dest='verbose', action='store_true', help='Verbose output') # Use if you want print output

    args = parser.parse_args()
    in_file  = args.in_file
    out_folder = args.out_folder
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    verbose  = args.verbose


#### Loading input file
    input_param = tools.load_h5_file(in_file,
                                     load=('CNN_Results/Baseline',
                                           'CNN_Results/Conc',
                                           'CNN_Results/Fs',
                                           'CNN_Results/MetabNames',
                                           'CNN_Results/ParamNames',
                                           'CNN_Results/Npt',
                                           'CNN_Results/Parameters',
                                           'CNN_Results/NMRFreq',
                                           'CNN_Results/WINDOW_START',
                                           'CNN_Results/WINDOW_END',
                                           'CNN_Results/N1',
                                           'CNN_Results/N2',
                                           'CNN_Results/Data_f',
                                           'CNN_Results/Data_f_full',
                                           'CNN_Results/Energy'),
                                     verbose=verbose)

    # Constantes
    Fs            = input_param['CNN_Results/Fs']
    Npt           = input_param['CNN_Results/Npt']
    NMRFreq       = input_param['CNN_Results/NMRFreq']
    WINDOW_START  = input_param['CNN_Results/WINDOW_START']
    WINDOW_END    = input_param['CNN_Results/WINDOW_END']
    N1            = input_param['CNN_Results/N1']
    N2            = input_param['CNN_Results/N2']
    Energy        = input_param['CNN_Results/Energy']

    # Arrays
    Parameters     = input_param['CNN_Results/Parameters'][:]
    Baseline       = input_param['CNN_Results/Baseline'][:]
    Concentration  = input_param['CNN_Results/Conc'][:]
    MetabNames     = input_param['CNN_Results/MetabNames'][:]
    ParamNames     = input_param['CNN_Results/ParamNames'][:]
    Data_f      = input_param['CNN_Results/Data_f'][:]
    Data_f_full = input_param['CNN_Results/Data_f_full'][:]

    ppm = np.linspace(WINDOW_START, WINDOW_END, Npt)[N1:N2] 

#### Getting the normalized concentration value and names
    metab_name_str = tools.decode_array_ascii(MetabNames)
    param_name_str = tools.decode_array_ascii(ParamNames)



#### Plotting Spectrum
    '''
    The following shows an example of how to reconstruct a spectrum with the evaluated parameters and how to plot on top of the in-vivo spectrum.
    '''

    #geting index of highest atp voxel
    df =  pd.Series(Parameters,index=param_name_str) 

    Spectra, S_meta = tools.creating_spec_from_param(Concentration,df,Baseline,metab_name_str,Npt,N1,N2,Fs,NMRFreq)

    Data_reph = tools.rephase_spec(Data_f,df,Baseline,Npt,N1,N2,Fs,True)
    Spec = tools.rephase_spec(Spectra,df,Baseline,Npt,N1,N2,Fs,False)

    Spec_meta = np.zeros((S_meta.shape), dtype=np.complex64)
    for ii in range(S_meta.shape[1]):
        Spec_meta[:,ii] = tools.rephase_spec(S_meta[:,ii],df,Baseline,Npt,N1,N2,Fs,False)



    tools.plot_raw(Data_f,Spectra,Baseline,ppm,out_folder)

    tools.plot_short(Data_reph,Spec,ppm,out_folder)

    tools.plot_long(Data_reph,Spec,Spec_meta,metab_name_str,ppm,out_folder)

    tools.appreciation()

if __name__ == '__main__':
    main()

