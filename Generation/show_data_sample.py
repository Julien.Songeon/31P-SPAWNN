#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################

#### Python imports
import sys, os
import argparse
from functools import partial
import glob
import h5py
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from numpy.random import rand, randn, seed
import pandas as pd
import pickle
import seaborn as sns
import time
from tqdm import tqdm

#### Self functions imports
sys.path.append('../Tools')
import tools

#matplotlibdd.use('Qt5Agg')

def main():

#### Retrieving arguments
    parser = argparse.ArgumentParser(description='Generate dataset.')
    parser.add_argument('-i', '--input', dest='in_file',required=True)      # Mandatory argument h5 file
    parser.add_argument('-o', '--output', dest='out_file',required=True)      # Mandatory argument pdf file
    parser.add_argument('--nevent', dest='nevent',default = 50)  
    parser.add_argument('--whichdata', dest='whichdata',default = 'train')  

    args = parser.parse_args()
    in_file   = args.in_file
    out_file  = args.out_file
    nevent    = int(args.nevent)
    whichdata = args.whichdata

    '''
    Input 'whichdata' is either 'train' or 'test' and select random spectra to display. 
    This script is mostly for visual checking.
    '''
    
 
    data = tools.load_h5_file(in_file, 
                            load=(whichdata+'/spectra_clean',
                                  whichdata+'/BL_spectra',
                                  whichdata+'/spectra',
                                  whichdata+'/amplitudes',
                                  whichdata+'/SNR',
                                  whichdata+'/Lorwidth',
                                  whichdata+'/Gauwidth',
                                  whichdata+'/Voigtwidth',
                                  whichdata+'/freqshift',
                                  whichdata+'/phshift',
                                  whichdata+'/acqdelay',
                                  whichdata+'/index')
                             verbose=False)

    # Constantes 
    Fs           = data[whichdata+'/spectra/Fs'] 
    Npt          = data[whichdata+'/spectra/Npt']
    NMRFreq      = data[whichdata+'/spectra/NMRFreq']
    WINDOW_START = data[whichdata+'/spectra/WINDOW_START']
    WINDOW_END   = data[whichdata+'/spectra/WINDOW_END']
    N1           = data[whichdata+'/spectra/N1']
    N2           = data[whichdata+'/spectra/N2']

    # Variables
    spectra       = data[whichdata+'/spectra'][:]
    spectra_clean = data[whichdata+'/spectra_clean'][:]
    spectra_BL    = data[whichdata+'/BL_spectra'][:]
    SNR           = data[whichdata+'/SNR'][:]
    Lorwidth      = data[whichdata+'/Lorwidth'][:]
    Gauwidth      = data[whichdata+'/Gauwidth'][:]
    Voigtwidth    = data[whichdata+'/Voigtwidth'][:]
    FreqShift     = data[whichdata+'/freqshift'][:]
    phshift       = data[whichdata+'/phshift'][:]
    acqdelay      = data[whichdata+'/acqdelay'][:]
    amplitudes    = data[whichdata+'/amplitudes'][:]
    names_metab        = data[whichdata+'/names']

    names_param = []
    for ii in range(len(names_metab)-2):
    #for ii in [0,1,2,3]:
        names_param.append('freqshift_'+names_metab[ii])
    names_param.extend(['freqshift spectrum','zero_phase','acq_delay'])
    for ii in [0,1,4,6,7,11]:
        names_param.append('Voigtwidth_'+names_metab[ii])
    for ii in [0,1,4,6,7,11]:
        names_param.append('Gauwidth_'+names_metab[ii])
    names_param.extend(['snr'])

    param_val = np.c_[FreqShift,phshift,acqdelay,Voigtwidth,Gauwidth,SNR]


    ppm = np.linspace(WINDOW_START, WINDOW_END, Npt)[N1:N2]

#### Plotting the spectra
    pdf = PdfPages(out_file)

    fig, ax = plt.subplots(nrows=4)
    fig.set_size_inches(12, 16)
    plt.tight_layout()
    ax = ax.flatten()
    sns.set(style="whitegrid",font_scale=1.5)

    def draw_spectra(ii):

        # Get the concentrations
        amp_text = ''
        for ij in range(len(names_metab)):
            if ij!=0: amp_text = amp_text +',\t'
            if ij%7==0: amp_text = amp_text +' \n'
            amp_text = amp_text + '{} = {:1.3f}mM'.format(names_metab[ij],amplitudes[ii,ij])

        par_text = ''
        for ij in range(len(names_param)):
            if ij!=0: par_text = par_text +',\t'
            if ij%5==0: par_text = par_text+' \n'
            if "frequshift" in names_param[ij] or "width" in names_param[ij]:
                par_text = par_text +  '{} = {} Hz'.format(names_param[ij],np.round(param_val[ii,ij],3))
            elif "acq_delay" in names_param[ij]:
                par_text = par_text +  '{} = {} s'.format(names_param[ij],np.round(param_val[ii,ij],6))
            else:
                par_text = par_text +  '{} = {}'.format(names_param[ij],np.round(param_val[ii,ij],3))


        tot_text = amp_text+' \n\n '+par_text
        tot_text = tot_text.expandtabs(8)

       
        ax[0].clear()
        ax[0].set_title('Real part', fontsize=10)
        ax[0].plot(ppm,np.real(spectra[ii,:]), '-C7')
        ax[0].plot(ppm,np.real(spectra_clean[ii,:]), '--r')
        ax[0].plot(ppm,np.real(spectra_BL[ii,:]), ':b')
        ax[0].legend(['Spectrum','Spectrum clean','Baseline'], fontsize=10)
        ax[0].set_xlabel('ppm', fontsize=10)
        ax[0].xaxis.set_tick_params(labelsize=10)
        ax[0].yaxis.set_tick_params(labelsize=10)
        ax[0].invert_xaxis()
        ax[0].grid(True)
        ax[1].clear()
        ax[1].set_title('Imaginary part', fontsize=10)
        ax[1].plot(ppm,np.imag(spectra[ii,:]), '-C7')
        ax[1].plot(ppm,np.imag(spectra_clean[ii,:]), '--r')
        ax[1].plot(ppm,np.imag(spectra_BL[ii,:]), ':b')
        ax[1].legend(['Spectrum','Spectrum clean','Baseline'], fontsize=10)
        ax[1].set_xlabel('ppm', fontsize=10)
        ax[1].xaxis.set_tick_params(labelsize=10)
        ax[1].yaxis.set_tick_params(labelsize=10)
        ax[1].invert_xaxis()
        ax[1].grid(True)
        ax[2].clear()
        ax[2].set_title('Absolute part', fontsize=10)
        ax[2].plot(ppm,np.abs(spectra[ii,:]), '-C7')
        ax[2].plot(ppm,np.abs(spectra_clean[ii,:]), '--r')
        ax[2].plot(ppm,np.abs(spectra_BL[ii,:]), ':b')
        ax[2].legend(['Spectrum','Spectrum clean','Baseline'], fontsize=10)
        ax[2].set_xlabel('ppm', fontsize=10)
        ax[2].xaxis.set_tick_params(labelsize=10)
        ax[2].yaxis.set_tick_params(labelsize=10)
        ax[2].invert_xaxis()
        ax[2].grid(True)
        ax[3].clear()
        ax[3].text(0,0.5,tot_text, fontsize=10)
        ax[3].get_xaxis().set_visible(False)
        ax[3].get_yaxis().set_visible(False)
        ax[3].axis('off')
        ax[3].grid(False)
        plt.tight_layout()

   
    # Select randomely nevent spectra from the total 
    if nevent==spectra.shape[0]:
        spectra_selected = np.array(range(0,nevent))
    elif nevent<spectra.shape[0]:
        seed()
        spectra_selected = np.random.randint(spectra.shape[0], size=nevent)
        spectra_selected.sort()
    else:
        print('ERROR: size of event selected exceed the size of the data created')

    with tqdm(total=spectra_selected.shape[0], desc='Plotting Spectra') as pbar:
        for jj in range(spectra_selected.shape[0]):
            draw_spectra(spectra_selected[jj])
            pdf.savefig(fig)
            plt.close()
            pbar.update(1)

        pdf.close()
       
    tools.appreciation()

if __name__ == '__main__':
    main()
