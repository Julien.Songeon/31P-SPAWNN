#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


#### Python imports
import sys, os
import argparse
import datetime
import h5py
import matplotlib.pyplot as plt
import numpy as np
import time


#### Self functions imports
sys.path.append('../Tools')
import tools
import gene_data


def main():
    time_initial = time.time()

#### Retrieving the arguments
    parser = argparse.ArgumentParser(description='Generate dataset.')
    parser.add_argument('-o', '--output', dest='filename', required=True, help='Ouput filename (filename.h5)')
    parser.add_argument('--wstart', dest='WINDOW_START', default=-40, 	help='highest ppm value of the frequency window (default: %(default)s)')
    parser.add_argument('--wend', dest='WINDOW_END', default=40, 	help='lowest ppm value of the frequency window (default: %(default)s)')
    parser.add_argument('--ntrain', dest='nbex_train', default=1000000, help='Number of realizations for training (default: %(default)s)')
    parser.add_argument('--ntest', dest='nbex_test', default=10000,	help='Number of realizations for testing (default: %(default)s)')
    parser.add_argument('--minsnr', dest='MinSNR', default=0.4,  	help='Minimal Signal to Noise Ratio (default: %(default)s)')
    parser.add_argument('--maxsnr', dest='MaxSNR', default=30.0,		help='Maximal Signal to Noise Ratio (default: %(default)s)')
    parser.add_argument('--maxFShft', dest='MaxFreq_Shift', default=50.0,help='Maximal Frequency Shift (default: %(default)s)')
    parser.add_argument('--maxPkW', dest='MaxPeak_Width', default=80.0,	help='Maximal peak width (FWHM*pi) (default: %(default)s)')
    parser.add_argument('--minPkW', dest='MinPeak_Width', default=1.0,	help='Minimal peak width (FWHM*pi) (default: %(default)s)')
    parser.add_argument('--Fs', dest='Fs', default=4000.0,		help='Sample Rate [Hz] (default: %(default)s)')
    parser.add_argument('--Npt', dest='Npt', default=2048,		help='Number of acquisition points (default: %(default)s)')
    parser.add_argument('--nBL', dest='NbBL', default=15,		help='Number of Gaussian functions in the Baseline (default: %(default)s)')
    parser.add_argument('--wBL', dest='MinBLWidth', default=500,	help='Min peak width of Baseline composents (default: %(default)s)')
    parser.add_argument('--AcqDel', dest='MaxAcqDelay', default=0.0006,	help='Maximum realized acquisition delay in s (default: %(default)s)')
    parser.add_argument('--nmrfreq', dest='NMRFreq', default=49897028,	help='Magnet Resonance Freq. (default: %(default)s)')
    parser.add_argument('--N1', dest='N1', default=-25,			help='Begining of narrow window in ppm (default: %(default)s)')
    parser.add_argument('--N2', dest='N2', default=10,			help='Ending of narrow window in ppm (default: %(default)s)')
    parser.add_argument('--mode', dest='mode_path', default='MetabModes/*Exact_Modes.txt', 	help='Ending of narrow window in ppm (default: %(default) s)')
    parser.add_argument('-sl', '--savelong', dest='savelong', action='store_true',		help='Save the complete spectra')
    parser.add_argument('-f', '--force', dest='update', action='store_true',			help='Force overwriting existing file')
    parser.add_argument('-v', dest='verbose', action='store_true',				help='Verbose output')


    args = parser.parse_args()
    filename = args.filename

    if os.path.isfile(filename):
        if not args.update:
            print('{} already exists, add -f to force overwriting'.format(filename))
            return
        else:
            os.remove(filename)


    WINDOW_START  = float(args.WINDOW_START) 
    WINDOW_END    = float(args.WINDOW_END)
    nbex_train    = int(args.nbex_train)
    nbex_test     = int(args.nbex_test)
    MinSNR        = float(args.MinSNR)
    MaxSNR        = float(args.MaxSNR)
    MaxFreq_Shift = float(args.MaxFreq_Shift)
    MaxPeak_Width = float(args.MaxPeak_Width)
    MinPeak_Width = float(args.MinPeak_Width)
    Fs            = float(args.Fs)
    Npt           = int(args.Npt)
    NbBL          = int(args.NbBL)
    MinBLWidth    = float(args.MinBLWidth)
    MaxAcqDelay   = float(args.MaxAcqDelay)
    NMRFreq       = float(args.NMRFreq)
    N1            = int(args.N1)
    N2            = int(args.N2)
    verbose       = args.verbose
    mode_path     = args.mode_path

    if not args.savelong:
        print('Long spectra will not be saved.')
    else:
        print('Long spectra will be saved.')

#### Convertion from ppm to position in the array 
    N1 = int(np.around(((N1-WINDOW_START) * 1e-6 * NMRFreq)* Npt / Fs))
    N2 = int(Npt + np.around(((N2-WINDOW_END) * 1e-6 * NMRFreq)* Npt / Fs))
    print('Data Energy calculated between point {} and point {} with length of {} from total length {}.'.format(N1,N2,N2-N1,Npt))


#### Generate the training set
    print('\n')
    if verbose:
        print('Generating training data ({} realizations)'.format(args.nbex_train))
   
    time_make_trainset = time.time()

    spectra, spectra_long, amplitudes, metab_spectra, index, spectra_clean, BL_spectra, freq_shift, ph_shift,  peakwidth_Voigt, peakwidth_Lor,peakwidth_Gau, snr, acqudelay = gene_data.MakeTrainingSpectralData31P(nbex_train, MaxSNR, MinSNR, MaxFreq_Shift, MaxPeak_Width, MinPeak_Width, NbBL, MinBLWidth, MaxAcqDelay, Fs, Npt, N1, N2, NMRFreq, 'train', mode_path, verbose)

    index = list(index.items())
    spectra_max = np.abs(spectra_clean[:]+BL_spectra[:]).max()
    spectra_energy = (np.abs((spectra_clean[:,:]+BL_spectra[:,:]) / spectra_max) ** 2).sum(axis=1).mean()
        

    # Saving the training set
    with h5py.File(filename, 'w-', libver='earliest') as f:
        train = f.create_group('train')
        dset = train.create_dataset('spectra', data=spectra)
        dset.attrs['Fs'] = Fs
        dset.attrs['Npt'] = Npt
        dset.attrs['max'] = spectra_max
        dset.attrs['NMRFreq'] = NMRFreq
        dset.attrs['WINDOW_START'] = WINDOW_START
        dset.attrs['WINDOW_END'] = WINDOW_END
        dset.attrs['N1'] = N1
        dset.attrs['N2'] = N2
        dset.attrs['energy'] = spectra_energy
        train.create_dataset('SNR', data=snr)  
        train.create_dataset('freqshift', data=freq_shift)  
        train.create_dataset('phshift', data=ph_shift)  
        train.create_dataset('Voigtwidth', data=peakwidth_Voigt)
        train.create_dataset('Lorwidth', data=peakwidth_Lor)  
        train.create_dataset('Gauwidth', data=peakwidth_Gau)
        train.create_dataset('acqdelay', data=acqudelay)
        train.create_dataset('amplitudes', data=amplitudes)
        train.create_dataset('metab_spectra', data=metab_spectra)
        train.create_dataset('index', data=index)
        train.create_dataset('BL_spectra', data=BL_spectra)
        dset = train.create_dataset('spectra_clean', data=spectra_clean)
        if args.savelong:
            dset = train.create_dataset('spectra_long', data=spectra_long)

    elapsed_train = np.round(time.time() - time_make_trainset, decimals=4)

#### Generate the test set
    if verbose:
        print('Generating testing data ({} realizations)'.format(args.nbex_test))

    time_make_testset = time.time()

    spectra, spectra_long, amplitudes, metab_spectra, index, spectra_clean, BL_spectra, freq_shift, ph_shift,  peakwidth_Voigt, peakwidth_Lor,peakwidth_Gau, snr, acqudelay = gene_data.MakeTrainingSpectralData31P(nbex_test, MaxSNR, MinSNR, MaxFreq_Shift, MaxPeak_Width, MinPeak_Width, NbBL, MinBLWidth, MaxAcqDelay, Fs, Npt, N1, N2, NMRFreq, 'test', mode_path, verbose)

    
    index = list(index.items())
  
    # Saving the testing set 
    with h5py.File(filename, 'a', libver='earliest') as f:
        test = f.create_group('test')
        dset = test.create_dataset('spectra', data=spectra)
        dset.attrs['Fs'] = Fs
        dset.attrs['Npt'] = Npt
        dset.attrs['max'] = spectra_max
        dset.attrs['NMRFreq'] = NMRFreq
        dset.attrs['WINDOW_START'] = WINDOW_START
        dset.attrs['WINDOW_END'] = WINDOW_END
        dset.attrs['N1'] = N1
        dset.attrs['N2'] = N2
        dset.attrs['energy'] = spectra_energy
        test.create_dataset('SNR', data=snr)  
        test.create_dataset('freqshift', data=freq_shift)  
        test.create_dataset('phshift', data=ph_shift)  
        test.create_dataset('Voigtwidth', data=peakwidth_Voigt)
        test.create_dataset('Lorwidth', data=peakwidth_Lor)  
        test.create_dataset('Gauwidth', data=peakwidth_Gau)
        test.create_dataset('acqdelay', data=acqudelay)
        test.create_dataset('amplitudes', data=amplitudes)
        test.create_dataset('metab_spectra', data=metab_spectra)
        test.create_dataset('index', data=index)
        test.create_dataset('BL_spectra', data=BL_spectra)
        test.create_dataset('spectra_clean', data=spectra_clean)
        if args.savelong:
            test.create_dataset('spectra_long', data=spectra_long)

    elapsed_test = np.round(time.time() - time_make_testset, decimals=4)

#### Sending news on telegram
    time_tot = np.round(time.time() - time_initial, decimals=4)

    Dico_msg = {1: "Data created: "+filename.split('/')[-1],
                2: "Total time of running script: "+str(datetime.timedelta(seconds=time_tot)),
                3: "Time to generate training set: "+str(datetime.timedelta(seconds=elapsed_train)),
                4: "Time to generate testing  set: "+str(datetime.timedelta(seconds=elapsed_test))}

    N1 = int(args.N1)
    N2 = int(args.N2)
    Dico_msg_2 = {1: "WINDOW_START: "+str(WINDOW_START),
                  2: "WINDOW_END: "+str(WINDOW_END),
                  3: "nbex_train: "+str(nbex_train),
                  4: "nbex_test: "+str(nbex_test),
                  5: "MinSNR: "+str(MinSNR),
                  6: "MaxSNR: "+str(MaxSNR),
                  7: "MaxFreq_Shift: "+str(MaxFreq_Shift),
                  8: "MaxPeak_Width: "+str(MaxPeak_Width),
                  9: "MinPeak_Width: "+str(MinPeak_Width),
                  10: "Fs: "+str(Fs),
                  11: "Npt: "+str(Npt),
                  12: "NbBL: "+str(NbBL),
                  13: "MinBLWidth: "+str(MinBLWidth),
                  14: "MaxAcqDelay: "+str(MaxAcqDelay),
                  15: "NMRFreq: "+str(NMRFreq),
                  16: "N1: "+str(N1),
                  17: "N2: "+str(N2),
                  18: "Are the long spectra saved? "+str(args.savelong)} 

    msg_to_send = ''
    msg_to_send += 'Script Gene_FID_withParam_31P.py has finished. \n \n'
    msg_to_send += 'Informations: \n'
    for keys in Dico_msg.keys():
        msg_to_send +='  %s \n'%(Dico_msg.get(keys))
    msg_to_send += '\n\n'
    msg_to_send += 'Options of creation: \n'
    for keys in Dico_msg_2.keys():
        msg_to_send +='  %s \n'%(Dico_msg_2.get(keys))


    print('\n\n'+msg_to_send) 

    tools.appreciation()


if __name__ == '__main__':
    main()
