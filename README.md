<a name="readme-top"></a>
[![Apache License][license-shield]][license-url]



<br />
<div align="center">
  <a href="https://cibm.ch">
    <img src="readme_doc/cibm_logo.png" alt="Logo" width="107" height="80">
  </a>

<h3 align="center">31P-SPAWNN:<br /><sup>31</sup>P-Spectral Analysis With Neural Networks</h3>

</div>



<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about">About</a>
      <ul>
        <li><a href="#reference-paper">Reference paper</a></li>
        <li><a href="#how-to-cite">How to cite</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a>
      <ul>
        <li><a href="#script-execution">Script execution</a></li>
        <li><a href="#creating-new-datasets">Creating new datasets</a></li>
        <li><a href="#training-new-networks">Training new networks</a></li>
        <li><a href="#evaluating-in-vivo-data">Evaluating in-vivo data</a></li>
        <li><a href="#tools">Tools</a></li>
        <li><a href="#datasets">Datasets</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>


## About
This package contains a set of python scripts for 31P-MRSI analysis
using deep learning. It includes the scripts for generating new datasets, training new neural networks,
as well as currently published networks. The flowchart bellow illustrates the analytics pipeline.

<br />
<div align="center">
    <img src="readme_doc/fig1.png" alt="fig1">
</div>




### Reference paper

The article can be found at the following link: https://doi.org/10.1002/mrm.29446.

### How to cite

Songeon, J, Courvoisier, S, Xin, L, et al. In vivo magnetic resonance 31P-Spectral Analysis With Neural Networks: 31P-SPAWNN. Magn Reson Med. 2022; 1-14. doi:10.1002/mrm.29446



## Getting Started

The script works with Python 3.6.



### Prerequisites
In order to run the scripts you will need to install python3 on your
computer or bash shell. Current programs works on Python 3.6.9.  You
will need at least the following packages. The current version of SPAWNN works with the following
packages

| Package        |  Version    |
| -----------    | ----------- |
| h5py           |  2.9.0      |
| Keras          |  2.3.1      |
| matplotlib     |  3.0.3      |
| numpy          |  1.16.2     |
| pandas         |  0.24.2     |
| scipy          |  1.4.1      |
| seaborn        |  0.9.1      |
| tensorflow-gpu |  1.13.1     |
| Package        |  Version    |

We are running our code on nvidia gpu with 11Go of vram.


| truc           | Version
| -----------    | ----------- |
| NVIDIA-SMI     | 460.91.03   |
| CUDA           | 11.2        |


### Installation
To download the package you need to execute for cloning with ssh:
```
git clone git@gitlab.unige.ch:Julien.Songeon/31P-SPAWNN.git
```
Or execute for cloning with html:
```
git clone https://gitlab.unige.ch/Julien.Songeon/31P-SPAWNN.git
```

<div align="right">
<p align="right">(<a href="#readme-top">back to top</a>)</p>
</div>


## Usage


### Script execution
The scripts use argparse to add argument. For example, to pass the argument -i for input
and -o for output and --an_option a number, write:
```
python3 SCRIPTNAME.py -i path/to/input_file.h5 -o path/to/output_file.h5 --an_option=500
```

Arguments with the option required=True are mandatory, the others have default values. Verbose option can be activated with `-v` most of the time. Options available can be checked with
```
python3 SCRIPTNAME.py -h
```


### Creating new datasets
To create new dataset, you can use the folder `Generation`.

*  The folder `MetabModes`contains the generated .txt files using the  GAMMA software library. The file `Metab_Mean_STD.txt` contains the values used for the mean relative concentration between the metabolites. 

*  `Gene_FID_withParam_31P.py` will generate a .h5 file with the training and testing spectra data. The script can be used as follow:

```
python3 Gene_FID_withParam_31P.py -o /path/to/output_file.h5 --modify_default_parameters
```

The output path is mandatory, others options are optional. For file size reasons, the 2048 points spectra are not automatically saved, only the cropped 899 points spectra are. Use the `-sl` option to save the full spectra as well.


*   `show_data_sample.py` will plot some of the created spectra.

### Training new networks
Training of network are made in the `Training` folder.

*   `train_metab.py`, `train_param.py`, and `train_bl.py`are the three files that train the three networks. They take as input the previously generated dataset and as output a folder to store the weights.

*   `modulable_CNN.py` contains the architecture for the networks.

*   `visual_callbacks.py` plots the graphs of the training convergence and loss in the output folder.

For example, new network training can be made with the command:

```
python3 train_metab.py -i /path/to/the/generated/simulated_data.h5 -o /path/to/output_folder/ --modify_default_parameters
```


### Evaluating in-vivo data

*  For evaluating the in-vivo datas with CNN, script are in the `Reconstruction` folder.
0. `SPAWNN_analysis.py` will analyse FID and 3D MRSI.
```
python3 SPAWNN_analysis.py -i Datasets/in_vivo_csi.mat -o Example_results/in_vivo_csi.h5
```
1. Plots can be made with `FID_plots_results.py` or `MRSI_plots_results.py` depending on which data you want to plot.
```
python3 MRSI_plots_results.py -i Example_results/in_vivo_csi.h5 -o Example_results/in_vivo_csi/
```


### Tools
The `Tools` folder contains useful functions for all of the scrpts above. They are place here for clarity.
*   `gene_data.py` is specifically used by `Gene_FID_withParam_31P.py`.
*   `tools.py` is a compilation of functions.

### Datasets
The `Datasets` contains example of in-vivo FID and 3D MRSI that can be use to understand how the SPAWNN works. The data obtain from the MRI are the twix .dat files and are converted to matlab .mat files using the mapVBVD toolbox (`https://github.com/CIC-methods/FID-A/tree/master/inputOutput/mapVBVD`).

### Example of results

The analysis with the example in-vivo dataset can be found in the `Example_results` folder. The in-vivo data analysed with SPAWNN are saved here with some plots.  

<div align="right">
<p align="right">(<a href="#readme-top">back to top</a>)</p>
</div>



## License

Distributed under the Apache License. See `LICENSE` for more information.

<div align="right">
<p align="right">(<a href="#readme-top">back to top</a>)</p>
</div>


## Contact

The code is provided to support reproducible research. For questions or issues, write to [julien.songeon@unige.ch](mailto:julien.songeon@unige.ch)

<div align="right">
<p align="right">(<a href="#readme-top">back to top</a>)</p>
</div>



## Acknowledgments

This study was supported by the Swiss National Science Foundation (320030_182658). We gratefully acknowledge NVIDIA Corporation for the donation of the Titan V GPU used for this research. Open access funding provided by Universite de Geneve.

<div align="right">
<p align="right">(<a href="#readme-top">back to top</a>)</p>
</div>



<!-- MARKDOWN LINKS & IMAGES -->
[license-shield]: https://img.shields.io/badge/license-Apache%202-blue?style=for-the-badge
[license-url]: https://gitlab.unige.ch/Julien.Songeon/31P-SPAWNN/blob/main/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/julien-songeon/



