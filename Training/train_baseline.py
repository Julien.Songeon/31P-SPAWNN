#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################

#### Python imports
import sys, os 
import argparse
import csv 
import datetime
import h5py
import importlib
import json
import matplotlib.pyplot as plt 
from matplotlib.backends.backend_pdf import PdfPages 
import numpy as np 
from numpy.random import rand, randn, seed
import pandas as pd 
from pathlib import Path 
import seaborn as sns 
import time
from tqdm import tqdm 

#### Self functions imports
sys.path.append('../Tools') 
import modulable_CNN
import tools 
import visual_callbacks

#### Keras imports
import keras
from keras import backend as K
from keras.utils.vis_utils import plot_model
from keras.callbacks import CSVLogger

#### Tensorflow import
import tensorflow as tf


def main():
    time_initial = time.time()

#### Retrieving arguments
    parser = argparse.ArgumentParser(description='Train a U-Net NN to detect Baseline.')
    parser.add_argument('-o', '--output', dest='model_name',required=True) 	# Mandatory argument  
    parser.add_argument('-i', '--intput', dest='TrainDataset',required=True)	# Mandatory argument

    parser.add_argument('--Nbatch', dest='Nbatch',default = 500)	# Nb of samples propagated through cnn at single time
    parser.add_argument('--Nepochs', dest='Nepochs',default = 30)	# Nb of iteration on all dataset
    parser.add_argument('--nNeuron', dest='nNeuron',default = 50)	# Nb of neurons in dense layers
    parser.add_argument('--nFilters', dest='nFilters',default = 16)	# Nb of filters in convolution layers
    parser.add_argument('--GPUpartition', dest='GPUpartition',default = 0)	# GPU management
    parser.add_argument('--dropOut', dest='dropOut',default = 0.2)	# Dropout value for drop layers
    parser.add_argument('--optimizer', dest='optimizer',default = 'adam')	# Algo of function optimization
    parser.add_argument('--tLayer', dest='tLayer',default = 'PReLU')	# Activation function
    parser.add_argument('--regularizer', dest='regularizer',default = False)	# Reweighting of layers during optimization
    parser.add_argument('--nevent_test', dest='nevent_test',default = 50)     #Nb of event tested
    parser.add_argument('-v', dest='verbose', action='store_true', help='Verbose output') # Use if you want print output


    args = parser.parse_args()
    model_name = args.model_name
    filename = args.TrainDataset

    Nbatch       = int(args.Nbatch)
    Nepochs      = int(args.Nepochs)
    nNeuron      = int(args.nNeuron)
    nFilters     = int(args.nFilters)
    GPUpartition = float(args.GPUpartition)
    dropOut      = float(args.dropOut)
    optimizer    = args.optimizer
    tLayer       = args.tLayer
    regularizer  = args.regularizer
    nevent_test = int(args.nevent_test)
    verbose       = args.verbose


    if not os.path.exists(model_name):
        os.makedirs(model_name)

#### Parametrize the GPU
    from keras.backend.tensorflow_backend import set_session
    config = tf.ConfigProto()

    if GPUpartition == 0 :
        config.gpu_options.allow_growth = True                                      #To allow GPU to manadge alone its memory
    else:
        config.gpu_options.per_process_gpu_memory_fraction = GPUpartition           #To fraction manually the GPU memory

    check = True
    while check:
        #Control if tensor can allocate request memory
        try:
            set_session(tf.Session(config=config))
            check = False
        except:
            print("Error during memory allocation")
            time.sleep(5)
            check = True


#### Loading the training set
    data_train = tools.load_h5_file(filename,
                                    load=('train/spectra',
                                          'train/amplitudes',
                                          'train/index',
                                          'train/phshift',
                                          'train/acqdelay',
                                          'train/Lorwidth',
                                          'train/Gauwidth',
                                          'train/SNR',
                                          'train/freqshift',
                                          'train/BL_spectra'),
                                    verbose = verbose)

    # Constantes
    Fs          = data_train['train/spectra/Fs']
    Npt         = data_train['train/spectra/Npt']
    NMRFreq     = data_train['train/spectra/NMRFreq']
    WINDOW_START= data_train['train/spectra/WINDOW_START']
    WINDOW_END  = data_train['train/spectra/WINDOW_END']
    N1          = data_train['train/spectra/N1']
    N2          = data_train['train/spectra/N2']
    index       = data_train['train/index']
    names        = tools.get_name_from_index(index)

    # Variables 
    spectra     = data_train['train/spectra'][:]
    BL_spectra  = data_train['train/BL_spectra'][:]

    # Data permutation
    seed()
    perm = np.random.permutation(spectra.shape[0])
    
    spectra    = spectra [perm,:]
    BL_spectra = BL_spectra [perm,:]


    # Stacking and normalizing input
    spectra_stack = np.stack((np.real(spectra), np.imag(spectra)), axis=-1)
    spectra_energy = np.sum(np.abs(spectra)**2, axis=1).mean()
    spectra_stack *= np.sqrt(1.0/spectra_energy)
    y_in =  spectra_stack				
    # correcting the padding
    pad_beg, pad_end = tools.padding_power_of_2(y_in.shape[1], 3, 8)
    pad_beg = int(pad_beg)
    pad_end = int(pad_end)
    y_in = np.pad(y_in, ((0,0),(pad_beg, pad_end),(0,0)), 'edge')
    y_in = np.pad(y_in, ((0,0),(0, 0),(1,0)), 'wrap')   
    y_in = np.expand_dims(y_in, axis = -1)		

    # Stacking and normalizing output
    BL_spectra_stack = np.stack((np.real(BL_spectra), np.imag(BL_spectra)), axis=-1)
    BL_spectra_stack *= np.sqrt(1.0/spectra_energy)
    y_out = BL_spectra_stack
    y_out = np.pad(y_out, ((0,0),(pad_beg, pad_end),(0,0)), 'edge')
    y_out = np.expand_dims(y_out, axis = -1)

#### Train the model
    model = modulable_CNN.SPAWNN_Bl(input_shape= (y_in.shape[1],y_in.shape[2],1,),
                                              tLayer=tLayer, 
                                              regularizer=regularizer,
                                              nFilters=nFilters,
                                              nNeuron=nNeuron,
                                              drop=dropOut)

    V_splits = 0.01
    Tloss = 'mse'
    opimizer = keras.optimizers.adam()

    model.compile(loss=Tloss,
                  optimizer= optimizer,
                  metrics=[tools.R2_BL])
    model.summary()

    plotter = visual_callbacks.trainPlotter(graphs=['loss','R2_BL'],
                                            save_graph=True,
                                            names = "Baseline",
                                            name_model= "%s/graf_train.png"%(model_name))

    csv_logger = CSVLogger("%s/epoch_log.csv"%(model_name), 
                           separator=',', 
                           append=False)


    callbacks = [plotter,
                 keras.callbacks.ReduceLROnPlateau(monitor='val_loss', patience=30, verbose=1), 
                 csv_logger,
                 keras.callbacks.EarlyStopping(monitor='val_loss', patience=40, verbose=1)
                 ]

    t = time.time()
    train_history = model.fit(y_in,
                              y_out,
                              epochs=Nepochs,
                              validation_split=V_splits,
                              batch_size=Nbatch,
                              callbacks= callbacks,
                              verbose=1
                              )

    elapsed = np.round(time.time() - t, decimals=4)             #Time to train the model
    print("Time to train : %s [s]"%(elapsed))
    train_history = train_history.history

    out_predict = model.predict(y_in)
    out_predict = np.squeeze(out_predict[:,pad_beg:(np.shape(out_predict)[1]-pad_end),0]+ 1j*out_predict[:,pad_beg:(np.shape(out_predict)[1]-pad_end),1])

#### Saving model
    Dico_value = {  "Model name :" : model_name,
                    "Data loaded :": filename,
                    "Time to train [s] :" : elapsed,
                    "Mini-batch size :" : Nbatch,
                    "Number of Epochs :" : Nepochs,
                    "Train set size :" : y_in.shape[0] * (1-V_splits),
                    "Valid set size :" : y_in.shape[0] * V_splits,
                    "Optimizer :" : opimizer,
                    "Loss function :" : Tloss,
                    "Number of parameters :" : model.count_params(),
                    "Last Valid loss value :" : train_history['val_loss'][-1],
                    "Last Train loss value :" : train_history['loss'][-1],
                    "Valid losses values :" : train_history['val_loss'][:],
                    "Train losses values :" : train_history['loss'][:],
                    "Output names :" : names,
                    "DropOut Value :" : dropOut}

    with open("%s/Values.txt" %(model_name), "w") as log:
        log.write("INTERESTING VALUES \n")
        for keys in Dico_value.keys():
            log.write('%s %s \n' % ( keys , Dico_value.get(keys) ))


    #Save Weights
    model.save_weights("%s/weights.h5" %(model_name), overwrite=True)

    #Save Model Parameters
    with h5py.File("%s/params.h5" %(model_name), 'w', libver='earliest') as f:
        train = f.create_group('Model')
        train.create_dataset('Fs', data=Fs)
        train.create_dataset('Npt', data=Npt)
        train.create_dataset('spectra_energy', data=spectra_energy)
        train.create_dataset('NMRFreq', data=NMRFreq)
        train.create_dataset('WINDOW_START', data=WINDOW_START)
        train.create_dataset('WINDOW_END', data=WINDOW_END)
        train.create_dataset('N1', data=N1)
        train.create_dataset('N2', data=N2)
        train.create_dataset('pad_beg', data=pad_beg)
        train.create_dataset('pad_end', data=pad_end)

    #model
    model_json = model.to_json()
    with open("%s/model.json" %(model_name), "w") as json_file :
        json_file.write(model_json)
    #Image of layer
    plot_model(model, to_file= "%s/Structur.png" %(model_name),
                show_shapes=True, show_layer_names=True)
    #Training History

    print("Saved model to disk in folder %s" %(model_name))
    print("Script to train baseline model is Over!")

#### Evaluate on training set
    print('Plotting examples from training set')
    t = time.time()

    r2 = tools.np_rscore_bl(BL_spectra_stack,out_predict)
    print('R^2 total:\t\t'+str((np.mean(r2[0])+np.mean(r2[1]))/2))
    print('R^2 real:\t\t'+str(np.mean(r2[0])))
    print('R^2 imaginary:\t\t'+str(np.mean(r2[1])))

    seed()
    spectra_for_plot = np.random.randint(BL_spectra_stack.shape[0], size=nevent_test)
    spectra_for_plot.sort()    

    spectra_train_plot     = np.zeros((nevent_test,BL_spectra_stack.shape[1],BL_spectra_stack.shape[2]), dtype=np.float64)
    BL_spectra_train_plot  = np.zeros((nevent_test,BL_spectra_stack.shape[1],BL_spectra_stack.shape[2]), dtype=np.float64)
    Out_predict_train_plot = np.zeros((nevent_test,BL_spectra_stack.shape[1]), dtype=np.complex64)
    r2_train_plot          = np.zeros((nevent_test,BL_spectra_stack.shape[2]))
    for ii in range(nevent_test):
        spectra_train_plot[ii,:,:]    = spectra_stack[spectra_for_plot[ii],:,:]
        BL_spectra_train_plot[ii,:,:] = BL_spectra_stack[spectra_for_plot[ii],:,:]
        Out_predict_train_plot[ii,:]  = out_predict[spectra_for_plot[ii],:]
        r2_train_plot[ii,0] = r2[0][spectra_for_plot[ii]]
        r2_train_plot[ii,1] = r2[1][spectra_for_plot[ii]]

    pdf = PdfPages("%s/evalOnTrainingSet.pdf" %(model_name))

    fig, ax = plt.subplots(nrows=2)
    fig.set_size_inches(12, 8)
    plt.tight_layout()
    ax = ax.flatten()

    ppm = np.linspace(WINDOW_START, WINDOW_END, Npt)[N1:N2]

    t = time.time()
    with tqdm(total=nevent_test, desc='plotting evaluation on training set') as pbar:
        for ii in range(nevent_test):
            ax[0].clear()
            ax[0].set_title('Real part - event n°'+str(spectra_for_plot[ii]), fontsize=10)
            ax[0].plot(ppm,spectra_train_plot[ii,:,0], '-C7')  
            ax[0].plot(ppm,BL_spectra_train_plot[ii,:,0], '--b')  
            ax[0].plot(ppm,np.real(Out_predict_train_plot[ii, :]), ':r')  
            ax[0].legend(['Input spectrum','Simulated baseline','Estimated baseline']) 
            ax[0].set_xlabel('ppm', fontsize=10) 
            ax[0].xaxis.set_tick_params(labelsize=10)
            ax[0].yaxis.set_tick_params(labelsize=10)
            ax[0].text(0.1, 0.9, '$R^2$={}'.format(np.around(r2_train_plot[ii,0],4)),transform=ax[0].transAxes,fontsize=25,bbox={'facecolor': 'r', 'alpha': 0.5})
            ax[0].grid(True)  
            ax[1].clear()
            ax[1].set_title('Imaginary part - event n°'+str(spectra_for_plot[ii]), fontsize=10)
            ax[1].plot(ppm,spectra_train_plot[ii,:,1], '-C7')  
            ax[1].plot(ppm,BL_spectra_train_plot[ii,:,1], '--b')  
            ax[1].plot(ppm,np.imag(Out_predict_train_plot[ii, :]), ':r')  
            ax[1].legend(['Input spectrum','Simulated baseline','Estimated baseline']) 
            ax[1].set_xlabel('ppm', fontsize=10) 
            ax[1].xaxis.set_tick_params(labelsize=10)
            ax[1].yaxis.set_tick_params(labelsize=10)
            ax[1].text(0.1, 0.9, '$R^2$={}'.format(np.around(r2_train_plot[ii,1],4)),transform=ax[1].transAxes,fontsize=25,bbox={'facecolor': 'r', 'alpha': 0.5})
            ax[1].grid(True)  
            plt.tight_layout()
            pdf.savefig(fig)
            plt.close()
            pbar.update(1)
    pdf.close()
    elapsed_eval = np.round(time.time() - t, decimals=4)
    print("Time to plot training evaluation : %s [s]"%(elapsed_eval))


#### Evaluate on testing set
    t = time.time()
    data_test = tools.load_h5_file(filename,
                                   load=('test/spectra',
                                         'test/BL_spectra'),
                                   verbose = verbose)

    # Variables 
    spectra_test     = data_test['test/spectra'][:]
    BL_spectra_test  = data_test['test/BL_spectra'][:]

    spectra_stack_test = np.stack((np.real(spectra_test), np.imag(spectra_test)), axis=-1)
    spectra_stack_test *= np.sqrt(1.0/spectra_energy)
    y_in_test =  spectra_stack_test
    # correcting the padding
    y_in_test = np.pad(y_in_test, ((0,0),(pad_beg, pad_end),(0,0)), 'edge')
    y_in_test = np.pad(y_in_test, ((0,0),(0, 0),(1,0)), 'wrap')
    y_in_test = np.expand_dims(y_in_test, axis = -1)

    BL_spectra_stack_test = np.stack((np.real(BL_spectra_test), np.imag(BL_spectra_test)), axis=-1)
    BL_spectra_stack_test *= np.sqrt(1.0/spectra_energy)

    out_predict_test = model.predict(y_in_test) 
    out_predict_test = np.squeeze(out_predict_test[:,pad_beg:(np.shape(out_predict_test)[1]-pad_end),0]+ 1j*out_predict_test[:,pad_beg:(np.shape(out_predict_test)[1]-pad_end),1])

    r2_test = tools.np_rscore_bl(BL_spectra_stack_test,out_predict_test)
    r2_test = np.asarray(r2_test)
    print('R^2 total:\t\t'+str((np.mean(r2_test[0,:])+np.mean(r2_test[1,:]))/2))
    print('R^2 real:\t\t'+str(np.mean(r2_test[0,:])))
    print('R^2 imaginary:\t\t'+str(np.mean(r2_test[1,:])))

    seed()
    spectra_for_plot = np.random.randint(BL_spectra_stack_test.shape[0], size=nevent_test)
    spectra_for_plot.sort()

    spectra_test_plot     = np.zeros((nevent_test,BL_spectra_stack_test.shape[1],BL_spectra_stack_test.shape[2]), dtype=np.float64)
    BL_spectra_test_plot  = np.zeros((nevent_test,BL_spectra_stack_test.shape[1],BL_spectra_stack_test.shape[2]), dtype=np.float64)
    Out_predict_test_plot = np.zeros((nevent_test,BL_spectra_stack_test.shape[1]), dtype=np.complex64)
    r2_test_plot          = np.zeros((nevent_test,BL_spectra_stack_test.shape[2]))
    for ii in range(nevent_test):
        spectra_test_plot[ii,:,:]    = spectra_stack_test[spectra_for_plot[ii],:,:]
        BL_spectra_test_plot[ii,:,:] = BL_spectra_stack_test[spectra_for_plot[ii],:,:]
        Out_predict_test_plot[ii,:]  = out_predict_test[spectra_for_plot[ii],:]
        r2_test_plot[ii,0] = r2_test[0,spectra_for_plot[ii]]
        r2_test_plot[ii,1] = r2_test[1,spectra_for_plot[ii]]

    pdf = PdfPages("%s/evalOnTestingSet.pdf" %(model_name))

    fig, ax = plt.subplots(nrows=2)
    fig.set_size_inches(12, 8)
    plt.tight_layout()
    ax = ax.flatten()

    ppm = np.linspace(WINDOW_START, WINDOW_END, Npt)[N1:N2]

    t = time.time()
    with tqdm(total=nevent_test, desc='plotting evaluation on testing set') as pbar:
        for ii in range(nevent_test):
            ax[0].clear()
            ax[0].set_title('Real part - event n°'+str(spectra_for_plot[ii]), fontsize=10)
            ax[0].plot(ppm,spectra_test_plot[ii,:,0], '-C7')
            ax[0].plot(ppm,BL_spectra_test_plot[ii,:,0], '--b')
            ax[0].plot(ppm,np.real(Out_predict_test_plot[ii, :]), ':r')
            ax[0].legend(['Input spectrum','Simulated baseline','Estimated baseline'])
            ax[0].set_xlabel('ppm', fontsize=10)
            ax[0].xaxis.set_tick_params(labelsize=10)
            ax[0].yaxis.set_tick_params(labelsize=10)
            ax[0].text(0.23, 0.9, '$R^2$={}'.format(np.around(r2_test_plot[ii,0],4)),transform=ax[0].transAxes,fontsize=25,bbox={'facecolor': 'r', 'alpha': 0.5})
            ax[0].grid(True)
            ax[1].clear()
            ax[1].set_title('Imaginary part - event n°'+str(spectra_for_plot[ii]), fontsize=10)
            ax[1].plot(ppm,spectra_test_plot[ii,:,1], '-C7')
            ax[1].plot(ppm,BL_spectra_test_plot[ii,:,1], '--b')
            ax[1].plot(ppm,np.imag(Out_predict_test_plot[ii, :]), ':r')
            ax[1].legend(['Input spectrum','Simulated baseline','Estimated baseline'])
            ax[1].set_xlabel('ppm', fontsize=10)
            ax[1].xaxis.set_tick_params(labelsize=10)
            ax[1].yaxis.set_tick_params(labelsize=10)
            ax[1].text(0.23, 0.9, '$R^2$={}'.format(np.around(r2_test_plot[ii,1],4)),transform=ax[1].transAxes,fontsize=25,bbox={'facecolor': 'r', 'alpha': 0.5})
            ax[1].grid(True)
            plt.tight_layout()
            pdf.savefig(fig)
            plt.close()
            pbar.update(1)
    pdf.close()
    elapsed_eval_test = np.round(time.time() - t, decimals=4)
    print("Time to plot testing evaluation : %s [s]"%(elapsed_eval_test))


    print('Plotting done, sending summary message')

#### Sending news on telegram
    time_tot = np.round(time.time() - time_initial, decimals=4)

    Dico_msg = {1: "Model name: "+model_name.split('/')[-2],
                2: "Total time of running script [s]: "+str(datetime.timedelta(seconds=time_tot)),
                3: "Time to train CNN [s]: "+str(datetime.timedelta(seconds=elapsed)),
                4: "Time to evaluate on training set [s]: "+str(datetime.timedelta(seconds=elapsed_eval)),
                5: "Time to evaluate on testing set [s]: "+str(datetime.timedelta(seconds=elapsed_eval_test))}

    Dico_msg_2 = {1: "Nbatch: "+str(Nbatch),
                  2: "Nepochs: "+str(Nepochs),
                  3: "nNeuron: "+str(nNeuron),
                  4: "nFilters: "+str(nFilters),
                  5: "GPUpartition: "+str(GPUpartition),
                  6: "dropOut: "+str(dropOut),
                  7: "optimizer: "+str(optimizer),
                  8: "tLayer: "+str(tLayer),
                  9: "regularizer: "+str(regularizer),
                  11: "nevent_test: "+str(nevent_test)}

    msg_to_send = ''
    msg_to_send += 'Script train_baseline.py has finished. \n \n'
    msg_to_send += 'Informations: \n'
    for keys in Dico_msg.keys():
        msg_to_send +='  %s \n'%(Dico_msg.get(keys))
    msg_to_send += '\n\n'
    msg_to_send += 'Options of creation: \n'
    for keys in Dico_msg_2.keys():
        msg_to_send +='  %s \n'%(Dico_msg_2.get(keys))


    print('\n\n'+msg_to_send)
    tools.appreciation()

if __name__ == '__main__':
    main()

