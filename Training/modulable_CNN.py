#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


from keras.models import Sequential, Model
from keras.layers import Dense, Flatten, Conv1D, Conv2D, PReLU, Dropout, Input
from keras.layers import Dropout, concatenate, MaxPooling2D, Activation, ReLU, MaxPooling2D, AveragePooling2D
from keras.layers import LeakyReLU, Softmax, Cropping2D, UpSampling2D
from keras import initializers as initializers

try: #regularizers module is not store in the same place depending on the version
    from keras.layers import regularizers
except ImportError:
    from keras import regularizers



def SPAWNN_Q(input_shape,output_shape, regularizer, nFilters, nNeuron, drop,endReLU):
    X_input = Input(input_shape,)

    X = Conv2D(filters = 2*nFilters, kernel_size = (11,2), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X_input)
    X = ReLU()(X)



    X = Conv2D(filters = 2*nFilters, kernel_size = (13,2), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X)
    X = ReLU()(X)



    X = Conv2D(filters = int(2.5*nFilters), kernel_size = (13,1), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X)
    X = ReLU()(X)
    X = MaxPooling2D(pool_size=(2,1), strides=None, padding='valid', data_format=None)(X) 



    X = Conv2D(filters = int(2.5*nFilters), kernel_size = (13,1), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X)
    X = ReLU()(X)
    X = MaxPooling2D(pool_size=(2,1), strides=None, padding='valid', data_format=None)(X)
    


    X = Conv2D(filters = 3*nFilters, kernel_size = (7,1), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X)
    X = ReLU()(X)
    X = MaxPooling2D(pool_size=(2,1), strides=None, padding='valid', data_format=None)(X)



    X = Conv2D(filters = 4*nFilters, kernel_size = (9,1), strides = 1, padding ='valid',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None),
                     bias_initializer='zeros')(X)
    X = ReLU()(X)
    X = MaxPooling2D(pool_size=(2,1), strides=None, padding='valid', data_format=None)(X)



    X = Flatten()(X)
    X = Dropout(drop)(X)



    X = Dense(int(nNeuron*1.5),kernel_initializer= initializers.glorot_normal(seed=0),  bias_initializer='zeros')(X)
    X = PReLU()(X)



    X = Dense(nNeuron,kernel_initializer= initializers.glorot_normal(seed=0),  bias_initializer='zeros')(X)
    X = PReLU()(X)



    X = Dense(int(nNeuron/2),kernel_initializer= initializers.glorot_normal(seed=0),  bias_initializer='zeros')(X)
    X = PReLU()(X)


    if regularizer == 'l2':
        Y = Dense(output_shape, kernel_initializer= initializers.glorot_normal(seed=0),  bias_initializer='zeros')(X)
    else:
        Y = Dense(output_shape, kernel_initializer= initializers.glorot_normal(seed=0),  bias_initializer='zeros',
        kernel_regularizer=regularizers.l2(0.01))(X)    
    if endReLU:
        Y = ReLU()(Y)


    model = Model(inputs = X_input, outputs = Y, name='SPAWNN_Q')
    return model

def SPAWNN_Bl(input_shape,tLayer, regularizer, nFilters, nNeuron, drop) :           
    X_input = Input(input_shape,)

    l1Conv1 = Conv2D(filters = 2*nFilters, kernel_size = (11,2), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(X_input)
    
    l1Conv1Act = PReLU(shared_axes=[1, 2])(l1Conv1)
    
    l1Conv2 = Conv2D(filters = 2*nFilters, kernel_size = (11,2), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l1Conv1Act)
    l1Conv2Act = PReLU(shared_axes=[1, 2])(l1Conv2)
    
    l1Maxpool = MaxPooling2D(pool_size=(2,1), strides=None, padding='same', data_format=None)(l1Conv2Act)
    
    l2Conv1 = Conv2D(filters = int(2.5*nFilters), kernel_size = (7,1), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l1Maxpool)
    l2Conv1Act = PReLU(shared_axes=[1, 2])(l2Conv1)
    
    l2Conv2 = Conv2D(filters = int(2.5*nFilters), kernel_size = (7,1), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l2Conv1Act)
    l2Conv2Act = PReLU(shared_axes=[1, 2])(l2Conv2)    
    
    l2Maxpool = MaxPooling2D(pool_size=(2,1), strides=None, padding='same', data_format=None)(l2Conv2Act)
    
    l3Conv1 = Conv2D(filters = 3*nFilters, kernel_size = (7,1), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l2Maxpool)   
    l3Conv1Act = PReLU(shared_axes=[1, 2])(l3Conv1)
    
    l3Conv2 = Conv2D(filters = 3*nFilters, kernel_size = (7,1), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l3Conv1Act)   
    l3Conv2Act = PReLU(shared_axes=[1, 2])(l3Conv2)    
                         
    l3Maxpool = MaxPooling2D(pool_size=(2,1), strides=None, padding='same', data_format=None)(l3Conv2Act)
    
    l4Conv1 = Conv2D(filters = 3*nFilters, kernel_size = (7,2), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l3Maxpool)   
    l4Conv1Act = PReLU(shared_axes=[1, 2])(l4Conv1)
    
    l4Conv2 = Conv2D(filters = 3*nFilters, kernel_size = (13,2), strides = 1, padding ='same',
                     data_format='channels_last',
                     kernel_initializer= initializers.he_normal(seed=None), 
                     bias_initializer='zeros')(l4Conv1Act)   
    l4Conv2Act = PReLU()(l4Conv2)    

    ##### upscaling back to image space
    
    l5up = UpSampling2D(size = (2,1))(l4Conv2Act)
    l5merge = concatenate([l3Conv2Act,l5up], axis = 3)
    l5Conv1 = Conv2D(int(2.5*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l5merge)
    l5Conv1Act = PReLU(shared_axes=[1, 2])(l5Conv1)
    l5Conv2 = Conv2D(int(2.5*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l5Conv1Act)
    l5Conv2Act = PReLU(shared_axes=[1, 2])(l5Conv2)
    
    
    l6up = UpSampling2D(size = (2,1))(l5Conv2Act)
    l6merge = concatenate([l2Conv2Act,l6up], axis = 3)
    l6Conv1 = Conv2D(int(2*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l6merge)
    l6Conv1Act = PReLU(shared_axes=[1, 2])(l6Conv1)
    l6Conv2 = Conv2D(int(2*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l6Conv1Act)
    l6Conv2Act = PReLU(shared_axes=[1, 2])(l6Conv2)
        
    l7up = UpSampling2D(size = (2,1))(l6Conv2Act)
    l7merge = concatenate([l1Conv2Act,l7up], axis = 3)
    l7Conv1 = Conv2D(int(2*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l7merge)
    l7Conv1Act = PReLU(shared_axes=[1, 2])(l7Conv1)
    l7Conv2 = Conv2D(int(2*nFilters), kernel_size = (7,2), padding = 'same', kernel_initializer = 'he_normal')(l7Conv1Act)
    l7Conv2Act = PReLU()(l7Conv2)  
    l7Conv3 = Conv2D(int(2*nFilters), kernel_size = (1,2), padding = 'valid', kernel_initializer = 'he_normal')(l7Conv2Act)
    l7Conv3Act = PReLU(shared_axes=[1, 2]
                      )(l7Conv3)    
    
    
    convEnd = Conv2D(1, 1, kernel_initializer = 'he_normal')(l7Conv3Act)
    convEndAct = PReLU(shared_axes=[1, 2])(convEnd) 
    
    print("pass model")
    # Create model
    model = Model(inputs = X_input, outputs = convEndAct, name='SPAWNN_Bl')
    return model 


