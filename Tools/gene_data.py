#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


#### Python imports
import sys, os
import glob
import h5py
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages 
from matplotlib.gridspec import GridSpec 
import numpy as np
from numpy import array as a
from numpy.random import rand, randn, seed
import pandas as pd
from sklearn.metrics import r2_score
import time
from tqdm import tqdm

#### Keras imports
import keras.backend as K
import keras
import tensorflow as tf

#### Self functions imports
sys.path.append('../Tools')
import tools


#### Making the generated dataset
def MakeTrainingSpectralData31P(NbEx=1000, 
                                MaxSNR=8.0, 
                                MinSNR=0.3, 
                                MaxFreq_Shift=20.0,
                                MaxPeak_Width=80.0, 
                                MinPeak_Width=1.0, 
                                NbBL=15,
                                MinBLWidth=300,
                                MaxAcquDelay=0.0006,
                                Fs = 4000,
                                N = 2048,
                                N1=0,
                                N2=2048,
                                NMRFreq=49897028,
                                TestTrain='', 
                                modes_filter='MetabModes/*Exact_Modes.txt', 
                                verbose=True ):

    """
    Args:
        NbEx(int)               : Number of realizations
        MinSNR(float)           : Minimal Signal to Noise Ratio
        MaxSNR(float)           : Maximal Signal to Noise Ratio
        MaxFreq_Shift(float)    : Maximal Frequency Shift
        MaxPeak_Width(float)    : Maximal Peak Width
    """

    if verbose:
        print('Generating data...')

    TimeSerie = np.zeros(( N), dtype=np.complex64)
    TimeSerieClean = np.zeros(( N), dtype=np.complex64)
    BLTimeSerie = np.zeros((NbBL, N), dtype=np.complex64)
    BLTimeSerie_temp = np.zeros(( N), dtype=np.complex64)

    Spectra = np.zeros((NbEx, (N2-N1)), dtype=np.complex64)
    Spectra_long = np.zeros((NbEx, N), dtype=np.complex64)
    SpectraClean = np.zeros((NbEx, (N2-N1)), dtype=np.complex64)
    BLSpectra = np.zeros((NbEx, (N2-N1)), dtype=np.complex64)

    Time = np.linspace(0, N - 1, N) / Fs 
    Frequencies = np.linspace(0, Fs, N)

    
    list_file = glob.glob(modes_filter)
    NbM = len(list_file)
    metabo_modes = [None] * NbM  # empty list of size NbM
    index = {}  # mapping of metabolite name to index

    mean_std_csv = pd.read_csv('MetabModes/Metab_Mean_STD.txt', header=None).values


    for i, v in enumerate(mean_std_csv[:, 0].astype(str)):
        index[ bytes(v.strip(), 'utf8') ] = i
    if verbose:
        print(index)
        print(mean_std_csv)

    mean_std = mean_std_csv[:, 1:].astype(np.float32)

    names = [None] * NbM
    for i, filename in enumerate(list_file):
        metabo_mode = pd.read_csv(filename, header=None, skiprows=[0]).values
        a = filename.find('3T_') + 3
        b = filename.find('_Exact')
        name = bytes(filename[a:b].strip(), 'utf8')
        names[index[name]] = name 
        metabo_modes[index[name]] = metabo_mode

    #Adding PME and PDE as the sum of the other metabolites
    index[bytes('PME','utf8')] = len(index)
    index[bytes('PDE','utf8')] = len(index)
    names.append(bytes('PME','utf8'))
    names.append(bytes('PDE','utf8'))
    names = tools.decode_array_ascii(names)


    #Creating random concentration values
    '''
    Values are randomely created around mean values. Low negative values have their signs changed,
    and then any negative value left are set to zero for the distribution.
    '''
    seed()
    Amplitude = mean_std[:, 0]*4 +  mean_std[:, 0]* 4 * randn(NbEx, NbM)
    Amplitude[Amplitude<-4] = -1*Amplitude[Amplitude<-4]
    Amplitude[Amplitude<0 ] *= -1. 		#Abs make all values positives


    #Variation of width for different T2
    '''
    Width are registred in Voigt and then split in gaussian and Lorenzian terms. Some metabolites have
    been set with the same width,such as ATPs. 
    '''
    expexted_width = np.array([10.,6.,6.,6.5,4.,4.,3.,10.,10.,11.,12.,33.])
    PeakWidth_Voigt = (MinPeak_Width + rand(NbEx, NbM) * (MaxPeak_Width - MinPeak_Width))*(expexted_width/np.mean(expexted_width))
    PeakWidth_Voigt [:,2], PeakWidth_Voigt [:,3],PeakWidth_Voigt [:,4],PeakWidth_Voigt [:,8],PeakWidth_Voigt [:,9],PeakWidth_Voigt [:,10] = PeakWidth_Voigt [:,1], PeakWidth_Voigt [:,1],PeakWidth_Voigt [:,5],PeakWidth_Voigt [:,7],PeakWidth_Voigt [:,7],PeakWidth_Voigt [:,7]
    PeakWidth_Lor   = (0.2* PeakWidth_Voigt) + rand(NbEx, 1) * ((0.95 * PeakWidth_Voigt) - (0.2* PeakWidth_Voigt))
    PeakWidth_Gau   = np.sqrt( ( PeakWidth_Voigt - (0.5346*PeakWidth_Lor) )**2 - 0.2166*PeakWidth_Lor**2 )



    #Variation in frequency shift
    '''
    The frequency shift is the sum of the frequency shift of the spectrum and the chemical shift of each metabolite.
    '''
    FreqShift_spec  = (rand(NbEx, 1)*2 - 1) * MaxFreq_Shift
    FreqShift       = (rand(NbEx,NbM)*2-1)*10.0
    FreqShift_for_crea = FreqShift + FreqShift_spec

    PhShift         = rand(NbEx, 1) * 2 * np.pi  # phase zero in radian
    AcquDelay       =  (rand(NbEx, 1)) * MaxAcquDelay #phase 1 in degree
    SNR = MinSNR + rand(NbEx, 1) * (MaxSNR - MinSNR)


    TestTrain = 'Make '+TestTrain+' spectral data'
    with tqdm(total=NbEx, desc=TestTrain) as pbar:
        for ex in range(NbEx):

            TimeSerieClean = 0*TimeSerieClean
            TimeSerieTemp  = 0*TimeSerieClean
            TempMetabData = np.zeros((len(metabo_modes), N), dtype=np.complex64)

            for f, mode in enumerate(metabo_modes):
                Freq = ((mode[:, 0]) * 1e-6 * NMRFreq)[...,None]
                for Nuc in range(len(Freq)):
                    TempMetabData[f, :] += mode[Nuc, 1][...,None] * np.exp(1j * mode[Nuc, 2][...,None]) * np.exp(2 * np.pi * 1j * (Time + AcquDelay[ex])  * Freq[Nuc])  

                TimeSerieTemp[:] = Amplitude[ex, f] * TempMetabData[f, :]* np.exp(1j * PhShift[ex])
                TimeSerieClean[:] += TimeSerieTemp[:] * np.exp( (Time* 1j * 2 * np.pi * (FreqShift_for_crea[ex,f]) ) + (- (np.square(Time)) * (np.square(PeakWidth_Gau[ex,f]))) + ((np.absolute(Time)) * (- PeakWidth_Lor[ex,f]) ))



            BLTimeSerie = 0*BLTimeSerie
            AmpGauss  = 5.0*Amplitude[ex,:].max()*rand(NbBL,1);
            TimeGauss = rand(NbBL,1)/MinBLWidth
            PPMG      = rand(NbBL,1)*50-15
            FreqGauss = -(PPMG) * 1e-6 * NMRFreq
            PhGaus    = 2 * np.pi * rand(NbBL,1)
            BLTimeSerie[:] = AmpGauss*np.exp(1j*PhGaus + Time * 2 * np.pi * 1j* FreqGauss - Time**2 /(TimeGauss**2))
            BLTimeSerie_temp[:] = np.sum(BLTimeSerie, axis=0)

            SpectrumTemp = np.fft.fft(TimeSerieClean[:],axis=0) 

            NCRand=(randn(N) + 1j * randn(N))
            TimeSerie[:] = TimeSerieClean[:] + BLTimeSerie_temp[:] + np.fft.ifft(SpectrumTemp.std()/0.65 / SNR[ex] * NCRand,axis=0)
            
            Spectra[ex,:]      = np.fft.fftshift(np.fft.fft(TimeSerie, axis=0))[N1:N2]
            Spectra_long[ex,:]      = np.fft.fftshift(np.fft.fft(TimeSerie, axis=0))
            SpectraClean[ex,:] = np.fft.fftshift(np.fft.fft(TimeSerieClean, axis=0))[N1:N2]
            BLSpectra[ex,:]    = np.fft.fftshift(np.fft.fft(BLTimeSerie_temp, axis=0))[N1:N2]
            
            pbar.update(1)

    MetabSpectra = np.fft.fft(TempMetabData, axis=1)[:,N1:N2]
    Amplitude = np.c_[Amplitude,
                      Amplitude[:,7]+Amplitude[:,8], 	#PME
                      Amplitude[:,9]+Amplitude[:,10]]  		#PDE
    for i in range(len(names)):  
        print(names[i]+' \t'+str(Amplitude[:,i].mean()))      

    FreqShift = np.c_[FreqShift[:,:],FreqShift_spec]
    PeakWidth_Voigt = np.c_[PeakWidth_Voigt[:,0],PeakWidth_Voigt[:,1],PeakWidth_Voigt[:,4],PeakWidth_Voigt[:,6],PeakWidth_Voigt[:,7],PeakWidth_Voigt[:,11]]
    PeakWidth_Lor   = np.c_[PeakWidth_Lor[:,0],PeakWidth_Lor[:,1],PeakWidth_Lor[:,4],PeakWidth_Lor[:,6],PeakWidth_Lor[:,7],PeakWidth_Lor[:,11]]
    PeakWidth_Gau   = np.c_[PeakWidth_Gau[:,0],PeakWidth_Gau[:,1],PeakWidth_Gau[:,4],PeakWidth_Gau[:,6],PeakWidth_Gau[:,7],PeakWidth_Gau[:,11]]

   
    return Spectra, Spectra_long, Amplitude, MetabSpectra, index, SpectraClean, BLSpectra, FreqShift, PhShift, PeakWidth_Voigt, PeakWidth_Lor,PeakWidth_Gau, SNR, AcquDelay

