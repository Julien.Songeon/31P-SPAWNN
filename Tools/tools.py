#!/usr/bin/python3

####################################################
#                                                  #
# 31P-SPAWNN                                       #
# reference paper doi: 10.1002/mrm.29446           #
# url: gitlab.unige.ch/Julien.Songeon/31P-SPAWNN   #
#      See READE.me for documentation              #
# last modification: 09.11.2022                    #
#                                                  #
####################################################


#### Python imports
import sys, os
import glob
import h5py
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages 
from matplotlib.gridspec import GridSpec 
import numpy as np
from numpy import array as a
from numpy.random import rand, randn, seed
import pandas as pd
import seaborn as sns
from sklearn.metrics import r2_score
import time
from tqdm import tqdm

#### Keras imports
import keras.backend as K
import keras
import tensorflow as tf


#### Calcul of metrics for evaluation of performance of CNN
'''
Computes the r-score for the training scripts inside the training phase with keras
and outside with numpy. The baseline r-score is a slight vatiation of the normal one taking into
account the dimentions. 
'''

# Calcul of metric using numpy
def np_rscore(x, y):
    return np.mean((x - np.mean(x, axis=0)) * (y - np.mean(y, axis=0)), axis=0) / (np.std(x, axis=0) * np.std(y, axis=0))
def np_R2(x,y):
    return np.mean( np_rscore(x,y)**2 )

# Calcul of metric using keras (work only with keras arrays used for training network)
def rscore(x, y):
    return K.mean((x - K.mean(x, axis=0)) * (y - K.mean(y, axis=0)), axis=0) / (K.std(x, axis=0) * K.std(y, axis=0))
def R2(y_true, y_pred):
    return K.mean(K.square(rscore(y_true, y_pred)))

# Calcul of metric for baseline fitting
def np_rscore_bl(bl,op):
    x = np.transpose(bl,(1,0,2))
    y = np.transpose(op,(1,0))
    real_r2 = np_rscore(x[:,:,0],np.real(y))**2
    imag_r2 = np_rscore(x[:,:,1],np.imag(y))**2
    return real_r2,imag_r2


def R2_BL(bl_true,bl_pred):
    r2_bl_real = K.square(rscore(K.transpose(bl_true[:,:,0,0]),K.transpose(bl_pred[:,:,0,0])))
    r2_bl_imag = K.square(rscore(K.transpose(bl_true[:,:,1,0]),K.transpose(bl_pred[:,:,1,0])))
    return K.mean((r2_bl_real+r2_bl_imag)/2, axis=0)





#### h5py ascii decoding to utf-8
def decode_array_ascii(ascii_array):
    utf8_array = []
    for kk in range(len(ascii_array)):
        utf8_array.append(ascii_array[kk].decode('UTF-8'))
    return utf8_array




#### Calculating the padding needed to ensure correct dimension with maxpooling
'''
For baseline cnn: the downscaling phase of the network imposes a power of 2 for the initial dimension, since
it needs an integer number of dimension.
'''
def padding_power_of_2(array_dim, nb_of_maxpooling, pad_min=0):
    mod_pow_2 = (array_dim+2*pad_min)%(2**nb_of_maxpooling)
    needed = 2**nb_of_maxpooling-mod_pow_2
    pad_end = np.ceil((2**nb_of_maxpooling-mod_pow_2)/2)
    pad_beg = needed - pad_end
    pad_beg = pad_beg + pad_min
    pad_end = pad_end + pad_min
    return pad_beg, pad_end





#### Scanning h5 files trees
def scan_hdf5(path, recursive=True, tab_step=2):
    def scan_node(g, tabs=0):
        print(' ' * tabs, g.name)
        for k, v in g.items():
            if isinstance(v, h5py.Dataset):
                print(' ' * tabs + ' ' * tab_step + ' -', v.name)
            elif isinstance(v, h5py.Group) and recursive:
                scan_node(v, tabs=tabs + tab_step)
            if v.attrs:
                keys = list(v.attrs.keys())
                for key in keys:
                    print(' ' * tabs + ' ' * (tab_step+tab_step) + ' -', key)
    print('\nTree in '+os.path.split(path)[-1]+':' )
    with h5py.File(path, 'r') as f:
        scan_node(f)

#### Loading h5file                                                                                 
def load_h5_file(filename, load=(), verbose=False):
    if not load:
        print('ERROR: You need to specify keys to load')
        return {}
    if not os.path.exists(filename):
        print('ERROR: File {} does not exist.'.format(filename))
        return {}

    if verbose==True: scan_hdf5(filename)

    with h5py.File(filename, 'r', libver='earliest') as f:
        loaded_data = {}

        if type(load)==str:
            loaded_data[load] = f[load][()]#.value
        else:
            for key in load:
                loaded_data[key] = f[key][()]#.value
                if f[key].attrs:
                    keys_attrs = list(f[key].attrs.keys())
                    for key_attrs in keys_attrs:
                        loaded_data[key+'/'+key_attrs] = f[key].attrs[key_attrs]
    return loaded_data

#### Retrieving names from index
def get_name_from_index(index_):
    temp = dict([(k.decode('utf8'), int(v)) for (k, v) in index_])
    names = [None] * len(temp)
    for k, v in temp.items():
        names[v] = k
    return names




#### Plotting greek letters
def greek(names):
    greek_names = names
    greek_names[np.where(np.array(names)=='aATP')[0][0]]='α-ATP'
    greek_names[np.where(np.array(names)=='bATP')[0][0]]='β-ATP'
    greek_names[np.where(np.array(names)=='gATP')[0][0]]='γ-ATP'
    return greek_names

#### Get Lorentzian value from Voigt and Gaussian
def get_lorwidth(voigt_width,gau_width):
    lor_width = (0.5346*voigt_width - np.sqrt((0.5346*gau_width)**2 + 0.2166*(voigt_width**2 - gau_width**2)))/(0.5346**2 - 0.2166)
    if type(lor_width)!=np.float64:
        lor_width[lor_width<0]=0.
    elif type(lor_width)==np.float64 and lor_width<0.:
        lor_width = 0.
    return lor_width

#### Print for ending script
def appreciation():
    print('\n\nScript executed.\n We wish you a pleasant day.\n')


#### No-negative
'''
Some parameters does not make sens to be negative, such as concentration of time delay between pulse and echo. 
Thus their values are clipped to zero here.
'''

#### For concentration of metabolites
def no_negative_concentration(out_predict,names,y_span,y_min):
    for ii in range(out_predict.shape[1]):
        out_predict[:,ii] = np.clip(out_predict[:,ii],0,None)
    return out_predict

#### For parameters
def non_negative_parameters(out_predict,names,y_span,y_min):
    let_neg_num = ['sin_zero_phase','cos_zero_phase']
    for ii in range(out_predict.shape[1]):
        if names[ii] in let_neg_num:
            out_predict[:,ii] = np.clip(out_predict[:,ii],y_min[ii],y_span[ii]+y_min[ii])
        elif 'freqshift' or 'first_phase' in names[ii]:
            continue
        else:
            out_predict[:,ii] = np.clip(out_predict[:,ii],0,None)
    return out_predict



#### Creating Spectrum for reconstruction
def creating_spec_from_param(Concentration_,Parameters_,Baseline_,metab_name_str,Npt_,N1_,N2_,Fs_,NMRFreq_):
    '''
    The purpose of this function is to create a spectrum using the parameters coming from the cnn model. It is almost the same as the one that generates the simulated spectra.
    '''

    Time        = np.linspace(0,Npt_-1,Npt_)/Fs_
    Frequencies = np.linspace(0,Fs_,Npt_)

    # Retreive the Lorentzian width
    '''
    The cnn is trained with Gaussian and voigt width, but spectra are generated with gaussian and lorenzian width.
    '''
    Gauwidth_var = []
    Lor_width_var = []
    for ii in [0,1,1,1,4,4,6,7,7,7,7,11]:
        Gauwidth_var.append(Parameters_['Gauwidth_'+metab_name_str[ii]])
        Lor_width_var.append(get_lorwidth(Parameters_['Voigtwidth_'+metab_name_str[ii]],Parameters_['Gauwidth_'+metab_name_str[ii]]))

    # retreive ppm position of each metabolite
    modes_filter='../Generation/MetabModes/*Exact_Modes.txt'
    list_file = glob.glob(modes_filter)
    metabo_modes = [None] *len(Concentration_[:-2]) #here me discarde PME and PDE as they are summed of others metab.
    for kk in range(len(metabo_modes)):
        metabo_mode = pd.read_csv([s for s in list_file if metab_name_str[kk] in s][0], header=None, skiprows=[0]).values
        metabo_modes[kk] = metabo_mode


    # Create the time serie of each resonance
    TempMetabData = np.zeros((len(metabo_modes), Npt_), dtype=np.complex64)
    for f, mode in enumerate(metabo_modes):
        Freq = ((mode[:, 0]) * 1e-6 * NMRFreq_)[...,None]
        for Nuc in range(len(Freq)):
            TempMetabData[f, :] += mode[Nuc, 1][...,None] * np.exp(1j * mode[Nuc, 2][...,None]) * np.exp(2 * np.pi * 1j * (Time+Parameters_['first_phase'])  * Freq[Nuc])


    
    TimeSerie      = np.zeros((Npt_), dtype=np.complex64) # Time serie final
    TimeSerie_temp = np.zeros((Npt_), dtype=np.complex64) # Time serie for each loop
    TS_meta = np.zeros((Npt_,len(metabo_modes)), dtype=np.complex64) # Time serie of each metabolite

    freqshift = []
    for f in range(len(metabo_modes)):
        freqshift.append(Parameters_['freqshift_'+metab_name_str[f]])

        TimeSerie_temp[:] = Concentration_[f] * TempMetabData[f, :]* np.exp(1j*np.arctan2(Parameters_['sin_zero_phase'],Parameters_['cos_zero_phase']))

        TS_meta[:,f] = TimeSerie_temp[:] * np.exp((Time*2*1j*np.pi*(freqshift[f]+Parameters_['freqshift spectrum'])) + (-(np.square(Time+Parameters_['first_phase']))*(np.square(Gauwidth_var[f]))) + ((np.absolute(Time+Parameters_['first_phase']))*(-Lor_width_var[f])))

        TimeSerie[:] += TS_meta[:,f]


    Spectra      = np.fft.fftshift(np.fft.fft(TimeSerie, axis=0))[N1_:N2_]
    S_meta = np.fft.fftshift(np.fft.fft(TS_meta[:],axis=0),axes=0)[N1_:N2_]


    Spectra       *= np.sqrt(1 / np.sum(np.abs(Spectra) ** 2))
    S_meta[:,:]*= np.sqrt(1 / np.sum(np.abs(S_meta[:,:]) ** 2))

    return Spectra, S_meta


#### Rephasing the spectrum
def rephase_spec(Spec_,Param_,Baseline_,Npt_,N1_,N2_,Fs_,Bl_sub=False):
    Time        = np.linspace(0.,Npt_-1,Npt_)/Fs_
    Frequencies = np.linspace(-0.5*Fs_,0.5*Fs_,Npt_)

    Baseline_ = np.concatenate((np.zeros((N1_), dtype=np.complex64),
                               Baseline_,
                               np.zeros((Npt_-N2_), dtype=np.complex64)),
                               axis=None)

    if len(Spec_)==899:
        Spec_ = np.concatenate((np.zeros((N1_), dtype=np.complex64),
                               Spec_,
                               np.zeros((Npt_-N2_), dtype=np.complex64)),
                               axis=None)
    if Bl_sub:
        Spec_ = Spec_ - Baseline_



    Spectra_ =  np.fft.fft(np.fft.ifft(Spec_[:] * np.exp(1j*-np.arctan2(Param_['sin_zero_phase'],Param_['cos_zero_phase']))*np.exp(-2*np.pi*1j*Param_['first_phase']*Frequencies), axis=0)*np.exp(-Time*2*1j*np.pi*Param_['freqshift spectrum']), axis=0)[N1_:N2_]


    return Spectra_




#### Plots
def plot_raw(data_,spec_,bl_,ppm_,outfolder_):

    pdf = PdfPages(outfolder_+"/plot_raw.pdf")
    fig = plt.figure()
    fig.set_size_inches(10,4.5)
    gs = GridSpec(2,1,height_ratios=[2,1])
    ax = []
    sns.set(style="whitegrid",font_scale=0.8)
    mr_init = np.where(ppm_<-20)[0][-1]


    ax.append(fig.add_subplot(gs[0,0]))
    ax.append(fig.add_subplot(gs[1,0]))

    cn_in,cn_out,ph_in,ph_out = ensure_ppm_centering(spec_,data_,mr_init)
    ppmi = np.where(ppm_<-1)[0][-1]  
    ppmf = np.where(ppm_>0)[0][0]    
    norm_data = np.max(data_[ppmi:ppmf])-np.min(data_[ppmi:ppmf])
    norm_spec = np.max(spec_[ppmi:ppmf])-np.min(spec_[ppmi:ppmf])


    ax[0].clear()
    ax[0].set_title('Plot of the raw unphased in-vivo data with cnn reconstruction', fontsize=10)
    ax[0].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data),'-k',linewidth=0.75)
    ax[0].plot(ppm_[mr_init:],np.real(spec_[cn_in:cn_out]/norm_spec + bl_[ph_in:ph_out]/norm_data),'-r',linewidth=0.75)
    ax[0].plot(ppm_[mr_init:],np.real(bl_[ph_in:ph_out]/norm_data),'--b',linewidth=0.75)
    ax[0].legend(['Spectrum','CNN reconstruction','Baseline'], fontsize=10)
    ymin_1, ymax_1 = ax[0].get_ylim()
    ax[1].clear()
    ax[1].set_title('Residual', fontsize=10)
    ax[1].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data)-np.real(spec_[cn_in:cn_out]/norm_spec-bl_[ph_in:ph_out]/norm_data),'-k',linewidth=0.75)
    axlim = np.max([np.abs(ymin_1),np.abs(ymax_1)])
    ax[1].set_ylim([-1*axlim,axlim])

    for ii in range(len(ax)):
        ax[ii].spines['left'].set_color('Black')
        ax[ii].spines['bottom'].set_color('Black')
        ax[ii].spines['top'].set_visible(False)
        ax[ii].spines['right'].set_visible(False)
        ax[ii].set_xlabel('ppm', fontsize=10)
        ax[ii].invert_xaxis()
        ax[ii].grid(False)
    
    plt.tight_layout()
    pdf.savefig(fig, dpi=600)
    plt.close()
    pdf.close()



def plot_short(data_,spec_,ppm_,outfolder_):

    pdf = PdfPages(outfolder_+"/plot_short.pdf")
    fig = plt.figure()
    fig.set_size_inches(10,4.5)
    gs = GridSpec(2,1,height_ratios=[2,1])
    ax = []
    sns.set(style="whitegrid",font_scale=0.8)
    mr_init = np.where(ppm_<-20)[0][-1]


    ax.append(fig.add_subplot(gs[0,0]))
    ax.append(fig.add_subplot(gs[1,0]))

    cn_in,cn_out,ph_in,ph_out = ensure_ppm_centering(spec_,data_,mr_init)
    norm_data = np.max(data_)
    norm_spec = np.max(spec_)


    ax[0].clear()
    ax[0].set_title('Plot of the rephased in-vivo data with cnn reconstruction', fontsize=10)
    ax[0].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data),'-k',linewidth=0.75)
    ax[0].plot(ppm_[mr_init:],np.real(spec_[cn_in:cn_out]/norm_spec),'-r',linewidth=0.75)
    ax[0].legend(['Spectrum','CNN reconstruction'], fontsize=10)
    ymin_1, ymax_1 = ax[0].get_ylim()
    ax[1].clear()
    ax[1].set_title('Residual', fontsize=10)
    ax[1].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data)-np.real(spec_[cn_in:cn_out]/norm_spec),'-k',linewidth=0.75)
    axlim = np.max([np.abs(ymin_1),np.abs(ymax_1)])
    ax[1].set_ylim([-1*axlim,axlim])

    for ii in range(len(ax)):
        ax[ii].spines['left'].set_color('Black')
        ax[ii].spines['bottom'].set_color('Black')
        ax[ii].spines['top'].set_visible(False)
        ax[ii].spines['right'].set_visible(False)
        ax[ii].set_xlabel('ppm', fontsize=10)
        ax[ii].invert_xaxis()
        ax[ii].grid(False)
    
    plt.tight_layout()
    pdf.savefig(fig, dpi=600)
    plt.close()
    pdf.close()




def plot_long(data_,spec_,spec_meta_,metab_name_str_,ppm_,outfolder_):

    pdf = PdfPages(outfolder_+"/plot_long.pdf")
    fig = plt.figure()
    fig.set_size_inches(10,12)
    gs = GridSpec(3,1,height_ratios=[2,1,3])
    ax = []
    sns.set(style="whitegrid",font_scale=0.8)
    mr_init = np.where(ppm_<-20)[0][-1]


    ax.append(fig.add_subplot(gs[0,0]))
    ax.append(fig.add_subplot(gs[1,0]))
    ax.append(fig.add_subplot(gs[2,0]))

    cn_in,cn_out,ph_in,ph_out = ensure_ppm_centering(spec_,data_,mr_init)
    norm_data = np.max(data_)
    norm_spec = np.max(spec_)


    ax[0].clear()
    ax[0].set_title('Plot of the in-vivo data with cnn reconstruction with each metabolite', fontsize=10)
    ax[0].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data),'-k',linewidth=0.75)
    ax[0].plot(ppm_[mr_init:],np.real(spec_[cn_in:cn_out]/norm_spec),'-r',linewidth=0.75)
    ax[0].legend(['Spectrum','CNN reconstruction'], fontsize=10)
    ymin_1, ymax_1 = ax[0].get_ylim()
    ax[1].clear()
    ax[1].set_title('Residual', fontsize=10)
    ax[1].plot(ppm_[mr_init:],np.real(data_[ph_in:ph_out]/norm_data)-np.real(spec_[cn_in:cn_out]/norm_spec),'-k',linewidth=0.75)
    axlim = np.max([np.abs(ymin_1),np.abs(ymax_1)])
    ax[1].set_ylim([-1*axlim,axlim])

    labels = metab_name_str_[:-2]
    for kk in range(spec_meta_.shape[1]):
        ax[2].plot(ppm_[mr_init:],np.real(spec_meta_[ph_in:ph_out,kk]/norm_spec)+kk*0.25, '-r',linewidth=0.75)
    ax[2].set_yticks(np.arange(0,3,0.25))
    ax[2].set_yticklabels(labels)

    for ii in range(len(ax)):
        ax[ii].spines['left'].set_color('Black')
        ax[ii].spines['bottom'].set_color('Black')
        ax[ii].spines['top'].set_visible(False)
        ax[ii].spines['right'].set_visible(False)
        ax[ii].set_xlabel('ppm', fontsize=10)
        ax[ii].invert_xaxis()
        ax[ii].grid(False)
    
    plt.tight_layout()
    pdf.savefig(fig, dpi=600)
    plt.close()
    pdf.close()





def ensure_ppm_centering(cnn_,pha_,mri_):

    cn_in = mri_
    cn_out = cnn_.shape[0]
    ph_in = mri_
    ph_out = pha_.shape[0]
    diff = np.where(pha_[:]==pha_[:].max())[0][0] - np.where(cnn_[:]==cnn_[:].max())[0][0]
    if diff>0:
        cn_in  = cn_in  - diff
        cn_out = cn_out - diff
    elif diff<0:
        ph_in  = ph_in  + diff
        ph_out = ph_out + diff

    return cn_in,cn_out,ph_in,ph_out




